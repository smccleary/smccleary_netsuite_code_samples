/**
 * @NApiVersion 2.x
 * @NScriptType Restlet
 * @NModuleScope Public
 * @NAmdConfig  /04-Fulfillments-Config.json
 */
define([
		'N/record',
		'N/search',
		'N/log',
		'N/runtime',
		'N/task',
		'N/url',
		'N/cache',
		'N/http',
		'N/https',
		'N/encode',
		'N/render',
		'N/query',
		'/SuiteScripts/ClearionFulfillments2.0/src/ClearionFulfillments_Controller'
	],
	/**
	 * @param {record} record
	 * @param {search} search
	 * @param {log} log
	 * @param {runtime} runtime
	 * @param {task} task
	 * @param {url} url
	 * @param {cache} cache
	 * @param {http} http
	 * @param {https} https
	 * @param {encode} encode
	 * @param {render} render
	 * @param {query} query
	 * @param {*} ctrl
	 */
	function (record, search, log, runtime, task, url, cache,
	          http, https, encode, render, query, ctrl) {

		/**
		 * the main controller for this context w/ injected netsuite dependencies
		 * @recordType {ClearionFulfillments_Controller|*}
		 */
		var controller = new ctrl.ClearionFulfillments_Controller({
			url: url,
			record: record,
			search: search,
			log: log,
			runtime: runtime,
			cache: cache,
			http: http,
			https: https,
			encode: encode,
			render: render
		}, null);

		var TESTING_POST_METHOD = function (args) {
			// load the customer
			var cust
			return args;
		};

		var isJsonError = function (str) {
			try {
				var obj = JSON.parse(str);
				if (!obj.hasOwnProperty('error')) return false;
			} catch (e) {
				return false;
			}
			return true;
		};

		/**
		 * locates a service declared in the main controller
		 *
		 * @param service {string} the name of the controller prototype method
		 * @param args {{}} the arguments to pass to the method
		 * @returns {{error: boolean, msg: string, data: {}}}
		 */
		var locateService = function (service, args) {
			// define a generic response object
			var response = {
				error: false,
				msg: "",
				data: {},
				logs: [],
			};

			// check to see if the service is the testing endpoint
			if (service === "TESTING_POST_METHOD") {
				try {
					var result = TESTING_POST_METHOD(args);
					response.error = false;
					response.msg = service;
					response.data = result;
				} catch (e) {
					response.error = true;
					response.msg = (e.message || e.details || e) || service + " returned an unknown exception";
					response.data = null;
				}

				return response;
			}

			// get the string signature of the service called (for debugging)
			var signature = "Controller." + service + "()";

			// make sure the service is defined
			if (typeof controller[service] === "function") {
				// run the located service, catching any errors in the process
				try {
					response.error = false;
					response.msg = service;
					response.data = controller[service](args);
					log.debug(signature + ":OK", "service executed successfully");
				} catch (e) {
					var errorText = typeof e === "string" ? e : (e.message || e.details || e);

					response.error = true;
					response.data = null;
					response.msg = errorText || signature + " returned an unknown exception";

					if (isJsonError(errorText)) {
						response = JSON.parse(errorText)
						log.error(signature + " Error", response.msg);
						log.error("Last Error Stacktrace", response.stack);
					} else {
						log.error(signature + " Error", errorText);
					}
				}
			} else {
				// return an error if the service does not exist
				response.error = true;
				response.msg = signature + " does not exist";
				response.data = null;
				log.error("404 - Service Not Found", response.msg);
			}

			// append the logs
			response.logs = controller.logs;

			return response;
		};

		/**
		 *
		 * @param context {context}
		 * @returns {*}
		 */
		function doPost(context) {
			// make sure the context and it's properties are valid
			if (!context || typeof context !== "object") {
				return "context is not defined and is not an object";
			}

			if (!context.hasOwnProperty("service")) {
				return "service name is not defined";
			}

			if (!context.hasOwnProperty("args")) {
				return "service arguments are not defined";
			}

			// make sure the controller is defined. if not return null
			if (!controller || typeof controller !== "object") {
				return "invalid controller instance";
			}

			// finally, run the service locator
			return locateService(context.service, context.args);
		}

		return {
			post: doPost
		};

	});

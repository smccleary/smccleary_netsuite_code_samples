import {
    AffiliateOrderAddr,
    APPROVED_ORDER_STATUSES_FOR_FULFILLMENT,
    BoxItem,
    COMMON_TRANSACTION_COLUMNS,
    Container,
    DeviceProfile,
    EasyPostMapping,
    Entity,
    Guid,
    IFulfillmentStatus,
    InventoryCount,
    ItemFulfiller,
    LabelManifest,
    LabelManifestLabel,
    Order,
    ShipGroup,
    Shipment,
    ShippingAddress,
    ShippingLabelAddress,
    ShippingLabelShipment,
    ShippingRate,
    Utils,
    Clearion_ENTITY
} from "./ClearionFulfillments_Classes";
import {CountRoll, EasyPostLabelManifest, EasyPostRate, IEasyPostAddressBook, IEasyPostOptions, IFulfillmentConfig, IFulfillmentCoreOperations, IFulfillmentManager, IFulfillmentModules, IFulfillRequest,} from "./ClearionFulfillments_Interfaces";

export class ClearionFulfillments_Controller implements IFulfillmentCoreOperations {
    modules: IFulfillmentModules;
    config: IFulfillmentConfig;

    constructor(modules: IFulfillmentModules, config: IFulfillmentConfig) {
        this.modules = modules;
        this.config = config;
    }

    getFulfillerInstance = (): ItemFulfiller => new ItemFulfiller(this.modules, this.config);

    GetLocations(): any[] {
        try {
            var locations = [];

            this.modules.search.create({
                type: 'location',
                filters: [
                    ['isinactive', 'is', 'F'],
                ],
                columns: ['name'],
            }).run().each((d) => {
                if (d) {
                    locations.push({
                        id: parseInt(d.id),
                        name: d.getValue('name')
                    });
                }
                return true;
            });

            return locations;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    SetCacheValue(args: { key: string, value: any }) {
        try {
            const cache = this.modules.cache.getCache({name: 'ff2.0'});
            cache.put({
                key: args.key,
                value: args.value,
                ttl: 300
            });
            return true;
        } catch (e) {
            throw e.details || e.message || e.toString();
        }
    }

    GetCacheValue(args: { key: string }) {
        try {
            const cache = this.modules.cache.getCache({name: 'ff2.0'});
            const val = cache.get({
                key: args.key,
                loader: () => null
            });

            try {
                return JSON.parse(val);
            } catch (e) {
                return val;
            }
        } catch (e) {
            throw e.details || e.message || e.toString();
        }
    }

    PrintLabelManifest(args: LabelManifestLabel): any {
        try {
            // todo: validate the params
            // todo: make sure the url is reachable
            // todo: submit the request
            // todo: parse the response
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    /**
     *
     * @param customer_id {number}
     * @returns {{carrier: string, account_number: string, postal_code: string}}
     */
    private lookup3rdPartyShippingAccount(customer_id: number): { account_number: string, postal_code: string, carrier: string } {
        try {
            let payor = null;

            this.modules.search.create({
                type: 'customrecordclearion_easypost_3rd_party_acct',
                columns: [
                    'name',
                    'custrecordclearion_easypost_3pa_acct_number',
                    'custrecordclearion_easypost_3pa_country',
                    'custrecordclearion_easypost_3pa_customer',
                    'custrecordclearion_easypost_3pa_postal_code',
                    'custrecordclearion_easypost_3pa_service',
                ],
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['custrecordclearion_easypost_3pa_customer', 'anyof', [customer_id]]
                ]
            }).run().each((r) => {
                if (r) {
                    payor = {};
                    payor.carrier = r.getText('custrecordclearion_easypost_3pa_service');
                    payor.account_number = r.getValue('custrecordclearion_easypost_3pa_acct_number');
                    payor.postal_code = r.getValue('custrecordclearion_easypost_3pa_postal_code');
                }
                return true;
            });

            if (!payor)
                throw `could not locate 3rd party shipping account with the customer id you supplied ${customer_id}`;

            return payor;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    /**
     *
     * @param args
     * @constructor
     */
    GetItemFulfillmentsFromOrder(args: { order_id: number }): { id: number, tranid: string, status: string }[] {
        try {

            let transactions = [];

            this.modules.search.create({
                type: 'itemfulfillment',
                columns: ['tranid', 'status'],
                filters: [
                    ['createdfrom', 'is', args.order_id],
                    'AND', ['mainline', 'is', 'T']
                ]
            }).run().each((r) => {
                if (r) {
                    transactions.push({
                        tranid: r.getValue('tranid'),
                        status: r.getValue('status'),
                        id: parseInt(r.id)
                    });
                }
                return true;
            });

            return transactions;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    CreateLabelsFromShipments(args: {
        order_id: number,
        carrier: string,
        service_type: string,
        check_readiness: boolean,
        return_files_only: boolean,
        test_mode: boolean,
        paid_by: string,
        paid_by_account: string,
        paid_by_postal_code: string,
        paper_size: string,
        service_options: any[],
        references: any[],
        containers: Container[],
        terms: Entity[],
    }): LabelManifest[] | LabelManifestLabel[] {
        try {
            // initialize the return variables
            let labels: LabelManifest[] = [];

            // lookup the account id to used based on the carrier provided
            const shipper_account = this.config.POSTMEN_SHIPPER_ACCTS.filter((acct) => acct.carrier === args.carrier)[0];

            // get the account number or error out
            if (typeof shipper_account === 'undefined' || shipper_account === null || !shipper_account)
                throw `could not lookup the shipper account id using the carrier name you provided: ${args.carrier}`;

            // get the rates from the carrier provided for the containers in this request
            const shipments: ShippingLabelShipment[] = this.CalculateShippingRates({
                carrier: args.carrier,
                test_mode: args.test_mode,
                get_shipments_only: true,
                check_readiness: args.check_readiness,
                order_id: args.order_id,
                containers: args.containers,
                terms: args.terms,
            });

            // determine who's account number and postal code to use depending on the payor
            let billing = {
                "paid_by": args.paid_by
            };

            // lookup the order details
            const order = this.LookupOrderDetails({
                id: args.order_id,
                append_addresses: false,
                append_items: false,
                append_shipments: false
            });

            // make sure the incoterms are set or error out
            if (typeof order.incoterm !== "string") {
                // default the incoterms to FOB if none were provided by the sales order
                order.incoterm = 'FOB';
            }

            if (args.paid_by === 'recipient' || args.paid_by === 'third_party') {
                // lookup the 3rd party account information
                const payor = this.lookup3rdPartyShippingAccount(order.entity);

                // update the billing method on the request
                billing['method'] = {
                    "type": "account",
                    "account_number": payor.account_number,
                    "postal_code": payor.postal_code
                }
            }

            let results = [];

            // we need to determine which references we need to send to the api as they
            // will differ depending on the carrier. this will override
            // the references that have been passed in
            let references = [];

            // fedex and ups are the only supporters of this metric for now
            if (args.carrier.toLowerCase().indexOf('fedex') >= 0) {
                references = ['P_O_NUMBER:' + order.tranid];
            } else if (args.carrier.toLowerCase().indexOf('ups') >= 0) {
                references = [order.tranid];
            }

            // iterate over each shipment and get labels from the postmen api
            shipments.map((shipment: ShippingLabelShipment) => {
                let request = {
                    "async": false,
                    "billing": billing,
                    "references": references,
                    "customs": {
                        "terms_of_trade": order.incoterm.toLowerCase(),
                        "eei": {
                            "type": "no_eei",
                            "ftr_exemption": "noeei_30_37_a",
                        },
                        "purpose": "merchandise"
                    },
                    "return_shipment": false,
                    "is_document": false,
                    "service_type": args.service_type,
                    "paper_size": args.paper_size || 'default',
                    "shipper_account": {
                        "id": shipper_account['id']
                    },
                    "shipment": shipment
                };

                this.modules.log.audit({
                    title: 'POST:' + this.config.POSTMEN_LABELS_URL,
                    details: JSON.stringify(request)
                });

                // perform the http request to the shipping service
                const result = this.modules.https.post({
                    url: this.config.POSTMEN_LABELS_URL,
                    headers: {
                        'Content-Type': 'application/json',
                        'postmen-api-key': this.config.POSTMEN_API_KEY,
                    },
                    body: JSON.stringify(request)
                });

                // shorthand and parse the json response
                let jsonBody = JSON.parse(result.body);

                // log the full result to the stack
                results.push(jsonBody);

                // check for any errors in the response and return
                if (typeof jsonBody['meta'] !== "undefined" && jsonBody['meta']['code'] !== 200) {
                    throw jsonBody['meta']['details'].map((obj) => obj['info']).join(', ');
                }

                // add the label to the stack if there are no errors
                labels.push(jsonBody['data']);
            });

            // if only the labels were requested, return them
            const manifest = labels.map((l: LabelManifest) => {
                if (typeof l.files !== 'undefined') return l.files.label;
            });

            if (args.return_files_only) return manifest;

            // otherwise, return the full response objects
            return results;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            this.modules.log.error({title: 'error', details: JSON.stringify(e, null, 4)});
            throw error;
        }
    }

    /**
     *
     * @param shipmethod
     * @returns {{country: string, currency: string}}
     */
    private getIsoInformationFroMapping(shipmethod: number) {
        try {
            let ISO_COUNTRY = 'USD', ISO_CURRENCY = 'USD';
            this.modules.search.create({
                type: 'customrecordclearion_easypost_mapping',
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['custrecordclearion_easypost_ship_method', 'is', shipmethod]
                ],
                columns: [
                    'custrecordclearion_easypost_iso_currency',
                    'custrecordclearion_easypost_iso_country'
                ]
            }).run().each((r) => {
                if (r) {
                    ISO_COUNTRY = r.getValue('custrecordclearion_easypost_iso_country');
                    ISO_CURRENCY = r.getValue('custrecordclearion_easypost_iso_currency');
                }
                return true;
            });

            return {
                currency: ISO_CURRENCY,
                country: ISO_COUNTRY
            };
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw `getCurrencyFromSubsidiary: ${error}`;
        }
    }

    CalculateShippingRates(args: {
        carrier: string,
        test_mode: boolean,
        get_shipments_only: boolean,
        check_readiness: boolean,
        order_id: number,
        containers: Container[],
        terms: Entity[],
    }): any | ShippingLabelShipment[] {
        try {
            const fulfiller = new ItemFulfiller(this.modules, this.config);

            // determine is this is a shipment composition request
            const gettingShipmentsOnly = typeof args.get_shipments_only === 'boolean'
                ? args.get_shipments_only
                : false;

            let quotes = [],
                shipments: ShippingLabelShipment[] = [],
                shipgroupAddrMap: { [key: string]: ShippingAddress } = {},
                containerMap: { [key: string]: Entity[] } = {},
                containers: { [key: string]: Container } = {},
                shipServiceMap: { [key: string]: string } = {},
                order: Order = this.LookupOrderDetails({
                    id: args.order_id,
                    append_addresses: true,
                    append_shipments: true,
                    append_items: true
                });

            // the addresses on this scope
            let addrFrom: ShippingLabelAddress = null,
                addrTo: ShippingLabelAddress = null,
                addrTerm: Entity = null;

            // if this is a 3rd party billing order
            const is3rdParty = fulfiller.is3rdParty(order.recordType, order.internalid);
            const isDropShip = fulfiller.isDropShip(order.recordType, order.internalid);

            // lookup the customer information on the order
            let customer = null, addrType = null;
            if (typeof order.entity !== "undefined") {
                customer = this.modules.search.lookupFields({
                    type: "customer",
                    id: order.entity,
                    columns: ['email', 'phone', 'billcountry', 'shipcountry', 'isperson']
                });

                if (!customer) throw `could not lookup the customer with ths id you provided: ${order.entity}`;

                addrType = customer['isperson'] === 'F' || customer['isperson'] === false ? "business" : "residential";
            }

            // default the address to the current location's address
            const addrLoc = fulfiller.getShippingAddressByLocation(order.location);

            // determine if this is drop ship or 3rd party
            if (is3rdParty || isDropShip) {
                // todo: at this point we need to make sure we use the customer's account number

                // lookup the customer/entity on the order
                const addr: ShippingAddress = fulfiller.getShippingAddressByCustomerID(order.entity);

                // get the customer's 3rd part account information
                addrFrom = {
                    "contact_name": addr.name || order.customer,
                    "phone": addr.phone || '(210) 678-3700',
                    "email": addr.email,
                    "street1": addr.street1,
                    "street2": addr.street2 || null,
                    "street3": addr.street3 || null,
                    "city": addr.city,
                    "country": this.config.ISO_MAPS[addr.country][0],
                    "type": addrType,
                    "postal_code": addr.zip,
                    "company_name": addr.company,
                    "state": addr.street3
                };
            } else {

                addrFrom = {
                    "contact_name": addrLoc.name || order.customer,
                    "phone": addrLoc.phone || '(210) 678-3700',
                    "email": addrLoc.email,
                    "street1": addrLoc.street1,
                    "street2": addrLoc.street2 || null,
                    "street3": addrLoc.street3 || null,
                    "city": addrLoc.city,
                    "country": this.config.ISO_MAPS[addrLoc.country][0],
                    "type": "business",
                    "postal_code": addrLoc.zip,
                    "company_name": addrLoc.company,
                    "state": addrLoc.state || null
                };
            }

            // store a list of the fields we have looked up to prevent duplicate lookups
            let fieldMaps = {};

            // map out the containers
            args.containers.forEach((c: Container) => {
                containers[c.id] = c;
                containerMap[c.id] = [];
                shipgroupAddrMap[c.id] = null;

                // lookup the ship method for this container to get the service name
                let shipItem = null;
                let shipMethodStr = c.ship_method.toString();
                if (!fieldMaps.hasOwnProperty(shipMethodStr)) {
                    shipItem = this.modules.search.lookupFields({
                        type: 'shipItem',
                        id: c.ship_method,
                        columns: ['displayname']
                    });
                    fieldMaps[shipMethodStr] = shipItem;
                } else {
                    shipItem = fieldMaps[shipMethodStr];
                }

                if (!shipItem || typeof shipItem['displayname'] != 'string')
                    throw `could not lookup the shipitem with the id you provided: ${c.ship_method}"`;

                shipServiceMap[c.id] = shipItem['displayname'];
            });

            // map out the ship groups and add them to their parent container map values
            args.terms.forEach((item: Entity) => {
                // find the target item on the order
                const itemRef: Entity = order.items.filter((i) => i.item === item.term)[0];

                // update the shipgroup map with the item's line address
                if (typeof itemRef != "undefined") {

                    shipgroupAddrMap[item.container_id] = {
                        "contact_name": order.customer,
                        "phone": customer['phone'],
                        "email": customer['email'],
                        "street1": itemRef.shipaddress1 || null,
                        "street2": itemRef.shipaddress2 || null,
                        "street3": itemRef.shipaddress3 || null,
                        "city": itemRef.shipcity,
                        "country": this.config.ISO_MAPS[addrLoc.country][0],
                        "type": addrType,
                        "postal_code": itemRef.shipzip,
                        "company_name": order.customer,
                        "state": itemRef.shipstate || null
                    };

                    // add the item to its parent container
                    containerMap[item.container_id].push(item);
                }
            });

            // store a map of the containers we have already looked up
            let boxMap = {};

            // iterate the containers and compose the parcels needed for the request
            Object.keys(containerMap).forEach((cid: string) => {
                let parcels = [],
                    weight = 0;

                const container = containers[cid];

                // lookup the container's box item id to get the dimensions
                let boxItem = null,
                    boxItemIdStr = container.box_item_item_id.toString();

                if (!boxMap.hasOwnProperty(boxItemIdStr)) {
                    // check if this is a short roll with has a box checked
                    const itemFields = this.modules.search.lookupFields({
                        type: 'item',
                        id: container.box_item_item_id,
                        columns: [
                            'itemid',
                            'weight',
                            'custitemclearion_width',
                            'custitemclearion_length',
                            'custitemclearion_height',
                            'custitemclearion_has_a_box',
                            'custitemclearion_production_type',
                        ],
                    });

                    // check if this is a hasabox item and compose a box item with it instead
                    if (itemFields && itemFields['custitemclearion_has_a_box']) {
                        boxItem = new BoxItem(
                            itemFields['itemid'],
                            container.box_item_item_id,
                            parseFloat(itemFields['weight']),
                            parseFloat(itemFields['custitemclearion_width']),
                            parseFloat(itemFields['custitemclearion_length']),
                            parseFloat(itemFields['custitemclearion_height']),
                        );
                    } else {
                        // otherwise lookup the default has a box item
                        boxItem = this.GetBoxItem({
                            box_item_id: container.box_item_item_id
                        });
                    }

                    boxMap[boxItemIdStr] = boxItem;
                } else {
                    boxItem = boxMap[boxItemIdStr];
                }

                // throw an error if we cannot get a box item id
                if (!boxItem)
                    throw `could not lookup the box item id using the id you supplied: ${container.box_item_item_id}`;

                // this will hold the aggregate of the weight of items
                weight = boxItem.weight;

                // remove duplicate items from the container
                let containerItems = containerMap[cid];

                // compose the parcel object
                let parcel = {
                    description: boxItem.item,
                    box_type: "custom",
                    weight: {
                        value: null,
                        unit: "lb"
                    },
                    dimension: {
                        width: boxItem.width,
                        height: boxItem.height,
                        depth: boxItem.length,
                        unit: "in",
                    },
                    items: containerItems.map((term) => {
                        if (term.container_id === cid) {
                            // find the target item on the order
                            const item: Entity = order.items.filter((item) => item.item === term.term)[0];

                            // return null if the item does not exist on the order
                            if (!item) return null;

                            // increment the parent container weight
                            weight += item.weight * term.qty;

                            return {
                                "description": item.description,
                                "origin_country": this.config.ISO_MAPS[item.origin_country][0],
                                "quantity": term.qty,
                                "price": {
                                    "amount": item.cost,
                                    "currency": this.config.ISO_MAPS[item.origin_country][1]
                                },
                                "weight": {
                                    "value": item.weight,
                                    "unit": "lb"
                                },
                                "sku": item.item,
                                "hs_code": item.hs_tariff_number
                            };
                        }
                    }).filter((f) => f != null)
                };

                // update the weight on the parcel object
                parcel.weight.value = Math.round(weight);
                parcels.push(parcel);

                // shorthand the shipment object
                const shipment: ShippingLabelShipment = {
                    "parcels": parcels,
                    "ship_from": addrFrom,
                    "ship_to": shipgroupAddrMap[container.id]
                };

                // add the shipments to the stack
                shipments.push(shipment);

                const request = {
                    "shipper_accounts": this.config.POSTMEN_SHIPPER_ACCTS.map((r) => {
                        if (r.carrier == args.carrier) return {id: r.id};
                    }).filter((ele) => ele != null),
                    "is_document": false,
                    "container_id": container.id,
                    "shipment": shipment,
                    "async": false
                };

                quotes.push(request);
            });

            // is we only want the shipments then return them now
            if (gettingShipmentsOnly) return shipments;

            // variable to hold the rates that will be returned to the client
            let rates: ShippingRate[] | any[] = [];

            // iterate over the quotes and process each one
            quotes.map((quoteRequest) => {
                // grab the container id to scope before we remove it from the iterable
                const cid = quoteRequest['container_id'];

                // remove the container id from the request before submitting
                delete quoteRequest['container_id'];

                // send off the quote request to postmen
                const res = this.modules.https.post({
                    url: this.config.POSTMEN_RATE_URL,
                    headers: {
                        'Content-Type': 'application/json',
                        'postmen-api-key': this.config.POSTMEN_API_KEY,
                    },
                    body: JSON.stringify(quoteRequest)
                });

                // iterate over all of the responses and compose the label rates
                JSON.parse(res.body)['data']['rates'].map((rate: ShippingRate) => {
                    if (rate.service_name && rate.service_type) {
                        // re-assign the container id
                        rate.container_id = cid;

                        // add the rate to the stack
                        rates.push(rate);
                    }
                });
            });

            return rates;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw JSON.stringify(e);
        }
    }

    UpdateFulfillmentShippingInfo(args: {
        item_fulfillment_id: number,
        tracking_code: string,
        packagedescr: string,
        sscc: string,
        packageweight: number,
        rate: number,
        box_item_item_id: number,
        shipment_id: string,
        label_url: string,
        original_shipgroup?: string,
        shipstatus?: string,
    }) {
        try {
            let rec = this.modules.record.load({
                type: 'itemfulfillment',
                id: args.item_fulfillment_id,
                isDynamic: true,
            });

            if (args.shipstatus)
                rec.setValue({
                    fieldId: 'shipstatus',
                    value: args.shipstatus
                });

            rec.selectLine({sublistId: "package", line: 0});

            rec.setCurrentSublistValue({
                sublistId: "package",
                fieldId: "packagetrackingnumber",
                value: args.tracking_code
            });

            rec.setCurrentSublistValue({
                sublistId: "package",
                fieldId: "packagedescr",
                value: `${args.packagedescr}`
            });

            rec.setCurrentSublistValue({
                sublistId: "package",
                fieldId: "packageweight", value: args.packageweight
            });

            rec.commitLine({sublistId: "package"});

            rec.setValue({
                fieldId: "custbodyclearion_sa_ship_rate",
                value: args.rate
            });

            rec.setValue({
                fieldId: "shippingcost",
                value: args.rate
            });

            if (args.original_shipgroup)
                rec.setValue({
                    fieldId: "custbodyclearion_original_shipgroup",
                    value: args.original_shipgroup
                });

            rec.setValue({
                fieldId: "custbodyclearion_sa_gross_weight",
                value: args.packageweight
            });

            // list of container types
            const STOCK_BOX_TYPES = {
                StockBox: 1,
                HasABox: 2,
                CustomBox: 3,
                BoxBundle: 4,
                Pallet: 5
            };

            if (args.box_item_item_id) {
                rec.setValue({
                    fieldId: "custbodyclearion_stock_box",
                    value: args.box_item_item_id
                });
            }

            rec.setValue({
                fieldId: "custbodyclearion_sa_num_package",
                value: 1
            });

            if (args.sscc)
                rec.setValue({
                    fieldId: "custbodyclearion_box_tote_id",
                    value: args.sscc
                });

            rec.setValue({
                fieldId: "custbodyclearion_sa_label_data",
                value: args.label_url
            });

            return rec.save();
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    BillSalesOrder(args: { order_id: number, is_test?: boolean }): EasyPostLabelManifest {
        try {
            let payment_method: string = null,
                terms: string = null,
                tranid: string = null,
                status: string = null,
                type: string = null,
                doNotBill: boolean = false,
                manifest = {
                    is_error_label: false,
                    order_id: null,
                    tracking_code: null,
                    postage_label: null,
                    msg: null
                };

            // make sure the order id is valid
            if (typeof args.order_id !== 'number')
                throw `order id provided is not a valid number`;

            // make sure the order is valid
            this.modules.search.create({
                type: 'transaction',
                columns: [
                    'tranid',
                    'terms',
                    'paymentmethod',
                    'status',
                    'custbodyclearion_accts_bill_no_autobill'
                ],
                filters: [
                    ['mainline', 'is', 'T'],
                    'AND', ['internalid', 'is', args.order_id],
                ]
            }).run().each((r: searchResult) => {
                if (r) {
                    terms = r.getText('terms') || null;
                    payment_method = r.getText('paymentmethod') || null;
                    tranid = r.getValue('tranid') || null;
                    type = r.recordType;
                    status = r.getText('status') || null;
                    doNotBill = r.getValue('custbodyclearion_accts_bill_no_autobill') || false;
                }
                return true;
            });

            // make sure we found a sales order
            if (!tranid) throw `could not locate a sales order withe the id you provided: ${args.order_id}`;

            // make sure we have either terms or a payment method
            if (!terms && !payment_method) throw `could not determine terms or payment method for order ${tranid}`;

            // determine if this order is eligible for a cash sale
            const isEligibleForCashSale: boolean = this.config.CASH_SALE_ELIGIBLE_PAYMENT_METHODS.indexOf(payment_method.toLowerCase()) >= 0;

            // determine if we can bill this order now
            if (!terms && !doNotBill && status === 'Pending Billing' && isEligibleForCashSale) {
                try {
                    // transform the sales order into a cash sale
                    const cashSale: record = this.modules.record.transform({
                        fromType: type,
                        fromId: args.order_id,
                        toType: 'cashsale',
                        isDynamic: true
                    });

                    // save the cash sale
                    const id = cashSale.save();

                    // lookup the document number and append to the manifest
                    const fields = this.modules.search.lookupFields({type: 'cashsale', columns: ['tranid'], id: id});
                    manifest.order_id = fields['tranid'];
                    manifest.is_error_label = false;
                    manifest.msg = `order ${tranid} has been billed successfully. new cash sale # is ${manifest.order_id}`;

                    // todo: dump out a success label

                    // update the sales order billing exception field
                    this.updateFields('salesorder', args.order_id, {
                        custbodyclearion_accts_bill_exception: null
                    });

                    // if we are testing in sandbox, remove the cash sales on success
                    if (args.is_test && this.modules.runtime.envType.toLowerCase() === 'sandbox')
                        this.modules.record.delete({type: 'cashsale', id: id});
                } catch (e) {
                    const error = e.details || e.message || e.toString();
                    const tpl = `^XA 
                        ^CF0,90 
                        ^FO50,50^FD***DO NOT SHIP***^FS 
                        ^CF0,45 
                        ^CF0,40 
                        ^FO50,150^FDERROR PROCESSING SHIPMENT^FS 
                        ^FO50,200^FDPackage ID: ${tranid}^FS 
                        ^CF0,20 
                        ^FO50,250^GB700,1,3^FS 
                        ^CFA,20 
                        ^FO50,300^FDScan the code below using the^FS 
                        ^FO50,340^FDRe-print Shipping Label tool on the^FS 
                        ^FO50,380^FDhandheld once issue is resolved^FS 
                        ^FO50,420^FDto get actual shipping labels^FS 
                        ^CFA,10 
                        ^FO50,500^GB700,1,3^FS 
                        ^FX Third section with barcode. 
                        ^BY3,2,270 
                        ^FO50,550^BC^FD${tranid}^FS 
                        ^XZ`;

                    manifest.is_error_label = true;
                    manifest.postage_label = {
                        is_raw: true,
                        object: "ShippingHoldLabel",
                        label_url: tpl
                    };
                    manifest.msg = error;

                    // update the sales order billing exception field
                    this.updateFields('salesorder', args.order_id, {
                        custbodyclearion_accts_bill_exception: error
                    });
                }
            }

            return manifest;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetOrdersAssignedToEmployee(args: { timeframe: string, employee: number }): Order[] {
        try {
            // make sure the employee id provided is correct
            if (typeof args.employee !== 'number' || args.employee === 0)
                throw `the employee id provided is not a valid number or is zero`;

            // make sure the time-frame is provided
            if (typeof args.timeframe !== 'string' || !args.timeframe.length) throw `the employee string provided is not a valid string`;

            // create the filters to override the get orders pending function
            let filters: any = ['AND', ['custbodyclearion_assigned_employee', 'anyof', [args.employee]]];

            // finally, run the function
            return this.GetOrdersPending({timeframe: args.timeframe, additional_filters: filters});
        } catch (e) {
            throw e.details || e.message || e.toString();
        }
    }

    GetOrdersPending(args: { timeframe: string, additional_filters?: string[] }): Order[] {
        try {
            let orders: Order[] = [];
            let filters = [
                ['trandate', 'within', args.timeframe],
                'AND', ['mainline', 'is', 'T'],
                'AND', ['status', 'noneof', this.config.RESTRICTED_ORDER_STATUSES],
                'AND', [
                    ['recordtype', 'is', 'salesorder'],
                    'OR', ['recordtype', 'is', 'transferorder']
                ]
            ];

            // check for any additional filters and append
            if (args.additional_filters) args.additional_filters.forEach((f: string) => filters.push(f));

            this.modules.search.create({
                type: 'transaction',
                columns: ['internalid', 'tranid', 'status', 'name'],
                filters: filters,
            }).run().each((r: searchResult) => {
                if (r) {
                    const order = new Order(
                        r.recordType,
                        parseInt(r.id),
                        r.getValue('tranid'),
                        [],
                        r.getValue('status'),
                        r.getText('name') || 'N/A'
                    );
                    orders.push(order);
                }
                return true;
            });

            return orders;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetOrderFulfillmentReadiness(args: IFulfillRequest): { ready: boolean, reasons: string } {
        let reasons: string[] = [],
            resolution: boolean = false,
            order: Order = null,
            fulfiller = new ItemFulfiller(this.modules, this.config);

        try {
            // try to load the order object
            order = this.LookupOrderDetails({
                id: args.order_id,
                append_addresses: true,
                append_items: true
            });

            // make sure the order is in a status to fulfill
            if (APPROVED_ORDER_STATUSES_FOR_FULFILLMENT.indexOf(order.status) === -1) {
                throw `this order's status (${order.status}) does not meet fulfillment requirements. the approved statuses are: ${APPROVED_ORDER_STATUSES_FOR_FULFILLMENT.join(', ')}`;
            }

            // make sure this order has items to process
            if (!order.items.length || typeof order.items === 'undefined' || !order.items)
                throw `there are no items on this order to process`;

            // check addresses. to and from must be defined if this is not a multiship order
            if (!order.ismultishipto && !order.to) throw `could not lookup the {to} address on this non-multiship order`;
            if (!order.ismultishipto && !order.from) throw `could not lookup the {from} address on this non-multiship order`;

            // make sure there is an logistic (easypost for now) mapping for the order's shipping method if this is not a multiship
            if (!order.ismultishipto) {
                // make sure there is an logistic mapping for the item's shipping method
                if (!fulfiller.GetEasyPostMapping(order.shipmethod))
                    throw `could not locate a logistics mapping for the shipping method id provided on the order: ${order.shipmethod}`;
            }

            // make sure that the shipgroup is provided on all of the containers if this is a multiship order
            if (order.ismultishipto) {
                for (let i = 0; i < args.containers.length; i++) {
                    const cont = args.containers[i];
                    if (typeof cont.shipgroup === 'undefined' || typeof cont.shipgroup !== 'number') {
                        throw `the {shipgroup} property is not defined for container #${cont.id}. this field is required for multi ship orders`;
                    }
                }
            }

            // if this is a multiship order, then each item must have address information applied to it
            if (order.ismultishipto) for (const item of order.items) {
                // make sure a description is provided
                if (typeof item.description !== 'string' || !item.description.length)
                    throw `description is not set for item ${item.item}. this is required for multiship orders`;

                // make sure the tariff information is set on both item fields
                if (typeof item.hs_tariff_number !== 'string' || !item.hs_tariff_number.length)
                    throw `clearion commodity code - hs_tariff_number not set for item ${item.item}. this is required for multiship orders`;

                if (typeof item.commodity_code !== 'string' || !item.commodity_code.length)
                    throw `clearion commodity code - commodity_code not set for item ${item.item}. this is required for multiship orders`;

                if (typeof item.commodity_code_id !== 'number' || !item.commodity_code_id)
                    throw `clearion commodity code - commodity_code_id not set for item ${item.item}. this is required for multiship orders`;

                // make sure there is a cost set for this item
                if (typeof item.cost !== 'number' || item.cost === 0)
                    throw `averagecost is not defined for item ${item.item}. this is required for multiship orders`;

                // validate the address fields
                if (typeof item.shipmethod !== 'number')
                    throw `shipmethod (Ship Via) is not defined for line item ${item.item}. this is required for multiship orders`;

                if (typeof item.shipaddress1 === 'undefined' || !item.shipaddress1.length || !item.shipaddress1)
                    throw `shipaddress1 is not defined for line item ${item.item}. this is required for multiship orders`;

                if (typeof item.shipcity === 'undefined' || !item.shipcity.length || !item.shipcity)
                    throw `shipcity is not defined for line item ${item.item}. this is required for multiship orders`;

                if (typeof item.shipstate === 'undefined' || !item.shipstate.length || !item.shipstate)
                    throw `shipstate is not defined for line item ${item.item}. this is required for multiship orders`;

                if (typeof item.shipzip === 'undefined' || !item.shipzip.length || !item.shipzip)
                    throw `shipzip is not defined for line item ${item.item}. this is required for multiship orders`;

                // make sure each item has dimensions set
                if (typeof item.weight !== 'number' || item.weight === 0)
                    throw `weight is not set on item ${item.item}. this is required for multiship orders`;

                if (typeof item.len !== 'number' || item.weight === 0)
                    throw `length is not set on item ${item.item}. this is required for multiship orders`;

                if (typeof item.width !== 'number' || item.weight === 0)
                    throw `width is not set on item ${item.item}. this is required for multiship orders`;

                if (typeof item.height !== 'number' || item.weight === 0)
                    throw `height is not set on item ${item.item}. this is required for multiship orders`;

                // make sure there is an logistic mapping for the item's shipping method
                if (!fulfiller.GetEasyPostMapping(item.shipmethod))
                    throw `logistics mapping not found for the shipping method id provided on line item ${item.item}: ${item.shipmethod}. this is required for multiship orders`;
            }

            resolution = true;
            reasons = [];
        } catch (e) {
            let reason = e.details || e.message || e.toString();
            resolution = false;
            reasons.push(reason);
        }

        return {
            ready: resolution,
            reasons: reasons.join(' | ')
        }
    }

    ReshipFulfillment(args: { item_fulfillment_id: number, box_item_id: number, force_reship?: boolean }): any {
        this.modules.log.debug(`START`, `ReshipFulfillment(${args.item_fulfillment_id})`);
        try {
            let fulfillment: record,
                self = this,
                // setup annotations for the memo population
                annotations = [],
                addressBook: IEasyPostAddressBook,
                parentOrderID: number,
                parentOrderType: string,
                easyPostOptions: IEasyPostOptions = new class implements IEasyPostOptions {
                    label_format = self.config.EASYPOST_DEFAULT_LABEL_TYPE;
                },
                fulfiller = new ItemFulfiller(this.modules, this.config),
                customerID: number,
                shipMethodID: number,
                shipMethod: string,
                shippingBox: BoxItem,
                totalItemWeight: number = 0,
                items: any[] = [],
                container: Container,
                rates: EasyPostRate[],
                selectedRate: EasyPostRate = null,
                shippingLabels: EasyPostLabelManifest[],
                initialStatus: string = null,
                tranID: string = null;

            // default the confirm flag if it was not sent
            args.force_reship = typeof args.force_reship !== 'boolean' ? false : args.force_reship;

            // load the item fulfillment
            fulfillment = this.modules.record.load({type: 'itemfulfillment', id: args.item_fulfillment_id});
            this.modules.log.debug(`-- set {fulfillment}`, `found fulfillment (${args.item_fulfillment_id})`);
            if (!fulfillment) throw `could not locate an item fulfillment with the id you provided: ${args.item_fulfillment_id}`;

            // set the document id
            tranID = fulfillment.getValue('tranid');
            this.modules.log.debug(`-- set {tranID}`, tranID);

            // check if this fulfillment is shipped already of the confirm flag is not set
            initialStatus = fulfillment.getText('shipstatus');
            if (initialStatus === 'Shipped' && !args.force_reship) throw `this item fulfillment (${tranID}) is already shipped. set the {args.force_reship} param to 'true' to force shipment regeneration`;

            // get the customer from the transaction
            customerID = parseInt(fulfillment.getValue('entity')) || null;
            if (!customerID) throw `could not lookup the customer on the transaction provided: ${args.item_fulfillment_id}`;
            this.modules.log.debug(`-- set {customerID}`, customerID);

            // load the shipping box provided. this will throw an error if it does not exist
            shippingBox = this.GetBoxItem({box_item_id: args.box_item_id});

            // lookup th shipping method on the order
            shipMethodID = parseInt(fulfillment.getValue('shipmethod')) || null;
            shipMethod = fulfillment.getText('shipmethod');
            if (!shipMethodID) throw `could not lookup the shipmethod on the transaction provided: ${args.item_fulfillment_id}`;
            this.modules.log.debug(`-- set {shipMethod}`, shipMethod);
            this.modules.log.debug(`-- set {shipMethodID}`, shipMethodID);

            // lookup the easy post mapping
            const easyPostMapping: EasyPostMapping = fulfiller.GetEasyPostMapping(shipMethodID);
            if (!easyPostMapping) throw `could not lookup an easypost mapping with the ship method provided: ${shipMethod}`;
            this.modules.log.debug(`-- set {easyPostMapping}`, easyPostMapping);

            // set the parent order id and type
            parentOrderID = parseInt(fulfillment.getValue('createdfrom'));
            parentOrderType = fulfillment.getValue('ordertype') === 'TrnfrOrd' ? 'transferorder' : 'salesorder';
            this.modules.log.debug(`-- set {parentOrderID}`, parentOrderID);
            this.modules.log.debug(`-- set {parentOrderType}`, parentOrderType);

            // lookup the default addresses on the fulfillment. we may need to override these later depending on shipping type
            addressBook = {
                to_address: fulfiller.getShippingAddressByID(fulfillment.getValue('shippingaddress_key')),
                from_address: fulfiller.getShippingAddressByLocation(),
            };
            this.modules.log.debug(`-- set {addressBook}`, addressBook);

            // check if this is 3rd party billing
            if (fulfiller.is3rdParty(parentOrderType, parentOrderID)) {
                this.modules.log.debug(`-- is3rdParty()`, true);
                annotations.push('3Prty');
                const customerAddr: ShippingAddress = fulfiller.getShippingAddressByCustomerID(customerID);
                const account = fulfiller.get3rdPartyAccount(customerID, easyPostMapping.service_name);

                easyPostOptions.bill_third_party_postal_code = account.postal_code;
                easyPostOptions.bill_third_party_account = account.account_number;
                easyPostOptions.bill_third_party_country = account.country;

                // set the buyer address to the customer since we are billing 3rd party
                addressBook.buyer_address = customerAddr;
                this.modules.log.debug(`-- updated`, `is3rdParty = true. setting buyer addr to customer addr`);
                this.modules.log.debug(`-- updated`,
                    `{easyPostOptions} have been updated: ${JSON.stringify(easyPostOptions, null, 4)}`);

                this.modules.log.debug(`-- set {easyPostOptions}`, easyPostOptions);
            }

            // if this is a drop ship we need to set the from address as the customer's and not clearion's
            if (fulfiller.isDropShip(parentOrderType, parentOrderID)) {
                this.modules.log.debug(`-- isDropShip()`, true);
                annotations.push('DrpShp');
                addressBook.return_address = fulfiller.getShippingAddressByCustomerID(customerID);
                this.modules.log.debug(`-- updated`, `isDropShip = true. setting return addr to customer addr`);
            }

            // get all of the items on the fulfillment. throw an error if we get no items which should never happen
            const totalItemCount = fulfillment.getLineCount({sublistId: 'item'});
            if (!totalItemCount) throw `there are no items on this item fulfillment: getLineCount('item') = 0`;
            this.modules.log.debug(`-- set {totalItemCount}`, totalItemCount);

            // iterate the items on the fulfillment to collect info needed: weight, etc
            for (let i = 0; i < totalItemCount; i++) {
                let sku = fulfillment.getSublistText({sublistId: 'item', fieldId: 'item', line: i}),
                    weight = parseFloat(fulfillment.getSublistValue({
                        sublistId: 'item',
                        fieldId: 'itemweight',
                        line: i
                    })) || null;

                // throw an error if the weight is not defined or 0
                if (!weight) throw `the weight for ${sku} is empty or zero. this must be provided to complete fulfillment`;

                // append the item weight to the totalItemWeight. we will need this to append to the container's weight
                totalItemWeight += weight;
            }

            this.modules.log.debug(`-- set {totalItemWeight}`, totalItemWeight);

            // create a container
            container = new Container(
                new Date().getTime().toString(),
                args.box_item_id,
                shipMethodID,
                shippingBox.length,
                shippingBox.width,
                shippingBox.height,
                shippingBox.weight + totalItemWeight,
                easyPostMapping.carrier_account,
                easyPostMapping.service_name,
                fulfiller.generateSSCC(),
                easyPostMapping.carrier_name
            );

            // request rates for our container
            rates = fulfiller.getRatesForContainer(container, addressBook, easyPostOptions);
            this.modules.log.debug(`-- set {rates}`, rates);

            // iterate over the rates returned and select the one we need
            rates.forEach((rate: EasyPostRate) => {
                if (rate.service === container.carrier_service_name) selectedRate = rate;
            });

            // throw an error if we did not locate a rate to select
            if (!selectedRate)
                throw `could not locate a rate for the shipping method provided: ${easyPostMapping.carrier_name} ${easyPostMapping.service_name}`;
            this.modules.log.debug(`-- set {selectedRate}`, selectedRate);

            // purchase the selected rate to get the shipping labels
            shippingLabels = fulfiller.purchaseRate(selectedRate);
            this.modules.log.debug(`-- set {shippingLabels}`, shippingLabels);

            // make sure we get some labels back or error out
            if (!shippingLabels || !Array.isArray(shippingLabels) || shippingLabels.length === 0)
                throw `we did not get any shipping labels back from the postage service (EasyPost) after purchasing the selected rate`;

            // update the item fulfillment with the shipping information
            fulfiller.updateShippingInformation(args.item_fulfillment_id, shippingLabels[0], container);
            this.modules.log.debug(`-- updated`, 'updated itemfulfillment\'s shipping information');

            // set the item fulfillment status to shipped. take note that this will save the item fulfillment record
            fulfiller.setStatus(IFulfillmentStatus.SHIPPED, args.item_fulfillment_id);
            this.modules.log.debug(`-- updated`, `'updated itemfulfillment's status to: ${IFulfillmentStatus.SHIPPED} (shipped)`);

            // finally, return the shipping labels so the client can print them
            return shippingLabels;
        } catch (e) {
            throw e.details || e.message || e.toString();
        }
    }

    GetBoxItem(args: { box_item_id: number }): BoxItem {
        try {
            let box: BoxItem = null;

            if (typeof args.box_item_id !== 'number' || isNaN(args.box_item_id))
                throw `You must supply a valid {box_item_id} to this function as 
                an integer. The type you supplied was null or invalid`;

            this.modules.search.create({
                type: 'item',
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', [
                        ['formulatext: {custitemclearion_production_type}', 'is', 'Shipping Box'],
                        'OR',
                        ['formulatext: {custitemclearion_production_type}', 'is', 'Custom Box']
                    ],
                    'AND', ['weight', 'isnotempty', ''],
                    'AND', ['custitemclearion_width', 'isnotempty', ''],
                    'AND', ['custitemclearion_length', 'isnotempty', ''],
                    'AND', ['custitemclearion_height', 'isnotempty', ''],
                    'AND', ['internalid', 'is', args.box_item_id],
                ],
                columns: [
                    'itemid', 'weight', 'custitemclearion_width',
                    'custitemclearion_length', 'custitemclearion_height', 'custitemclearion_production_type'
                ],
            }).run().each((r: searchResult) => {
                if (r) box = new BoxItem(
                    r.getValue('itemid'),
                    parseInt(r.id),
                    parseFloat(r.getValue('weight')),
                    parseFloat(r.getValue('custitemclearion_width')),
                    parseFloat(r.getValue('custitemclearion_length')),
                    parseFloat(r.getValue('custitemclearion_height')),
                );
                return true;
            });

            if (!box)
                throw `could not locate a Shipping Box item with the id you provided: ${args.box_item_id}`;

            return box;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetBoxItems(): BoxItem[] {
        try {
            let boxes = [];

            this.modules.search.create({
                type: 'item',
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['formulatext: {custitemclearion_production_type}', 'is', 'Shipping Box'],
                    'AND', ['weight', 'isnotempty', ''],
                    'AND', ['custitemclearion_width', 'isnotempty', ''],
                    'AND', ['custitemclearion_length', 'isnotempty', ''],
                    'AND', ['custitemclearion_height', 'isnotempty', ''],
                ],
                columns: ['itemid', 'weight', 'custitemclearion_width', 'custitemclearion_length', 'custitemclearion_height'],
            }).run().each((r: searchResult) => {
                if (r) boxes.push(new BoxItem(
                    r.getValue('itemid'),
                    parseInt(r.id),
                    parseFloat(r.getValue('weight')),
                    parseFloat(r.getValue('custitemclearion_width')),
                    parseFloat(r.getValue('custitemclearion_length')),
                    parseFloat(r.getValue('custitemclearion_height')),
                ));

                return true;
            });

            return boxes;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetPrintersForLocation(args: { location: number }): DeviceProfile[] {
        try {
            const location = (typeof args.location !== 'number') ? this.modules.runtime.getCurrentUser().location : args.location;

            // make sure we can get a location
            if (!location) throw `could not determine the user's location programmatically`;

            let printers: DeviceProfile[] = [];

            this.modules.search.create({
                type: 'customrecordclearion_mobile_config_profile',
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['custrecordclearion_mcp_location', 'anyof', [location]],
                    'AND', ['formulatext: {custrecordclearion_mcp_type}', 'is', 'Printer'],
                ],
                columns: [
                    'name',
                    'custrecordclearion_mcp_address',
                    'custrecordclearion_mcp_location',
                    'custrecordclearion_mcp_type',
                    'custrecordclearion_mcp_is_secure',
                    'custrecordclearion_mcp_port',
                ]
            }).run().each((r) => {
                if (r) {
                    printers.push({
                        id: parseInt(r.id),
                        name: r.getValue('name'),
                        address: r.getValue('custrecordclearion_mcp_address'),
                        location: r.getText('custrecordclearion_mcp_location'),
                        type: r.getText('custrecordclearion_mcp_type'),
                        is_secure: r.getValue('custrecordclearion_mcp_is_secure'),
                        port: parseInt(r.getValue('custrecordclearion_mcp_port')) || null,
                    });
                }
                return true;
            });

            // throw an error if we cannot get any printers
            if (!printers.length)
                throw `could not locate any Clearion Mobile Config Profile's of type [Printer] for your location id: ${location}`;

            return printers;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetShortRollLabel(args: { roll_id: string, quantity: number }): string {
        try {
            // make sure the roll id was provided
            if (typeof args.roll_id !== 'string') throw `the roll id provided was not a valid string`;

            args.quantity = typeof args.quantity === 'undefined' ? 1 : args.quantity;

            // lookup the roll to make sure its valid and to extract the information needed for their label
            let roll = null;
            this.modules.search.create({
                type: 'customrecordclearion_roll',
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['name', 'is', args.roll_id]
                ],
                columns: [
                    'name',
                    'custrecordclearion_roll_item',
                    'custrecordclearion_roll_item.custitemclearion_roll_type',
                    'custrecordclearion_roll_seccode'
                ]
            }).run().each((r) => {
                if (r) roll = {
                    name: r.getValue('name'),
                    custrecordclearion_roll_item: r.getText('custrecordclearion_roll_item'),
                    roll_type: r.getValue({join: 'custrecordclearion_roll_item', name: 'custitemclearion_roll_type'}) || null,
                    custrecordclearion_roll_seccode: r.getValue('custrecordclearion_roll_seccode') || null,
                };

                return true;
            });

            // make sure a roll was found and that we have the information we need on the roll
            if (!roll) throw `could not locate a roll record with the name you provided: ${args.roll_id}`;

            // make sure the roll type is defined
            if (!roll.roll_type) throw `the [Clearion ROLL TYPE] is not set on item: ${roll.name}`;

            let labelZpl = null, labelFormatName;

            // get the short roll label format
            const rollTypeFields = this.modules.search.lookupFields({
                type: 'customrecordclearion_roll_type',
                id: roll.roll_type,
                columns: ['custrecordclearion_rt_label_format_core', 'name']
            });

            // make sure we were able to load the roll type
            if (!rollTypeFields) throw `could not find a roll type with the ID you supplied: ${roll.roll_type}`;

            // make sure the field we need is not undefined
            if (!Array.isArray(rollTypeFields['custrecordclearion_rt_label_format_core']))
                throw `the field returned for custrecordclearion_rt_label_format_core is not an array`;

            if (typeof rollTypeFields['custrecordclearion_rt_label_format_core'][0] === 'undefined')
                throw `the [custrecordclearion_rt_label_format_core] for roll type [${rollTypeFields['name']}] is undefined`;

            // finally, assign the text value
            labelFormatName = rollTypeFields['custrecordclearion_rt_label_format_core'][0]['text'];

            // throw an error if we obtain an empty string
            if (!labelFormatName || !labelFormatName.length)
                throw `the [text] value for field [custrecordclearion_rt_label_format_core] was empty`;

            // get the short roll label format defined on the roll type
            this.modules.search.create({
                type: 'customrecordclearion_lf',
                columns: ['custrecordclearion_lf_code', 'name'],
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['name', 'is', labelFormatName]
                ],
            }).run().each((r) => {
                if (r) labelZpl = r.getValue('custrecordclearion_lf_code');
                return true;
            });

            // make sure some zpl was returned from the search
            if (!labelZpl)
                throw `the label format you requested does not have any zpl text set in the custrecordclearion_lf_code field`;

            // step through the fields in the {roll} object and replace in the zpl template
            for (const field in roll)
                labelZpl = labelZpl.replace(new RegExp(`\\\${roll.${field}}`, 'gmi'), roll[field]);

            // finally return the parsed zpl quantity
            let zpl = "";
            for (let i = 0; i < args.quantity; i++) zpl += labelZpl;
            return zpl;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    AdHocCountRoll(args: { count_roll: CountRoll, count_id: number }): { zpl: string, roll_id_new: string, memo: string } {
        try {
            this.modules.log.audit('AdHocCountRoll:args', JSON.stringify(args));

            if (typeof args.count_roll === 'undefined') throw `args.count_rolls was not defined`;
            if (typeof args.count_roll !== 'object') throw `args.count_rolls was not an object`;
            if (typeof args.count_id !== 'number') throw `args.count_id was not a valid number`;

            // load the count record referenced. throw an error if we cannot load it
            let columns = Object.keys(CountRoll.$fields);
            columns.push('internalid');
            let countRec = this.modules.search.lookupFields({
                type: 'customrecordclearion_inv_cnt_roll',
                id: args.count_id,
                columns: columns
            });

            if (!countRec) throw `could not locate a count (customrecordclearion_inv_cnt_roll) with the id you provided: ${args.count_id}`;

            // lookup the current roll id supplied
            let oldRollRecordID = null;
            const oldRollId = args.count_roll.custrecordclearion_inv_ct_roll_id;

            // make sure the roll id provided is valid
            if (typeof oldRollId !== 'number' && typeof oldRollId !== 'string') throw `the roll id provided is not a valid string`;
            if (!oldRollId.length) throw `the roll id you provided was an empty string`;

            this.modules.search.create({
                type: 'customrecordclearion_roll',
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['name', 'is', oldRollId]
                ],
                columns: ['name']
            }).run().each((r) => {
                if (r) oldRollRecordID = parseInt(r.id);
                return true;
            });

            // make sure we found the roll we were looking for
            if (!oldRollRecordID) throw `could not locate a roll with the name you provided: ${oldRollId}`;

            // get the current fields to copy over
            let newRollRecord = this.modules.record.copy({
                type: 'customrecordclearion_roll',
                id: oldRollRecordID,
                isDynamic: true
            });

            // generate a new roll id
            const newRollID = this.createRollID(true);

            // update some ancillary values accordingly
            newRollRecord.setValue({fieldId: 'name', value: newRollID});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_createdby', value: this.modules.runtime.getCurrentUser().id});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_createdfrom_roll', value: oldRollRecordID});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_seccode', value: this.createSecurityCode()});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_box_print_date', value: null});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_box_print_user', value: null});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_soldcustomer', value: null});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_registeredcustomer', value: null});
            newRollRecord.setValue({fieldId: 'custrecordclearion_roll_registereddate', value: null});

            newRollRecord.setText({fieldId: 'custrecordclearion_roll_createdprocess', text: 'HandHeld Ad Hoc Process'});
            newRollRecord.save();

            const memo = `Ad-Hoc Done: ${oldRollId} -> ${newRollID}`;

            // update the count record's adhoc flag
            const savedCntID = this.modules.record.submitFields({
                type: 'customrecordclearion_inv_cnt_roll',
                id: countRec['internalid'][0]['value'],
                values: {
                    [CountRoll.$fields.custrecordclearion_inv_ct_roll_need_adhoc]: true,
                    [CountRoll.$fields.custrecordclearion_inv_ct_roll_msg]: memo,
                    [CountRoll.$fields.custrecordclearion_inv_ct_roll_adhoc_roll_id]: newRollID,
                }
            });

            // inflate a label format for our new roll
            const zpl = this.GetShortRollLabel({roll_id: newRollID, quantity: 2});

            // return the inflated zpl
            return {
                memo: memo,
                zpl: zpl,
                roll_id_new: newRollID
            };
        } catch (e) {
            throw e.toString();
        }
    }

    GetCountRollsNeedingAdHocAdjustments(args: { count_id: number }): CountRoll[] {
        try {
            let rolls = [],
                fields = CountRoll.$fields,
                columns = Object.keys(fields);

            if (typeof args.count_id === 'undefined') throw `no valid count id was provided with your request`;

            columns.push('custrecordclearion_inv_ct_roll_count.custrecordclearion_inv_cnt_location');

            this.modules.search.create({
                type: 'customrecordclearion_inv_cnt_roll',
                columns: columns,
                filters: [
                    [fields.isinactive, 'is', 'F'],
                    'AND', [fields.custrecordclearion_inv_ct_roll_need_adhoc, 'is', 'T'],
                    'AND', [fields.custrecordclearion_inv_ct_roll_adhoc_roll_id, 'isempty', null],
                    'AND', ['custrecordclearion_inv_ct_roll_count', 'anyof', [args.count_id]
                    ],
                ],
            }).run().each((r: searchResult) => {
                if (r) rolls.push(new CountRoll(r));
                return true;
            });

            return rolls;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    /**
     *
     * @param {{count_id: number; note: string; details: string}} args
     * @returns {number}
     * @constructor
     */
    CreateInventoryCountBatch(args: { count_id: number; note: string; details: string }): number {
        try {
            if (typeof args.count_id !== 'number')
                throw `args.count_id was not provided as a valid number: ${args.count_id}`;

            if (typeof args.details !== 'string' || !args.details.length)
                throw `args.details was not provided as a valid number: ${args.details}`;

            let rec = this.modules.record.create({
                type: 'customrecordclearion_inv_cnt_batch',
                isDynamic: true,
            });

            rec.setValue({fieldId: 'custrecordclearion_inv_cnt_batch_count', value: args.count_id});
            rec.setValue({fieldId: 'custrecordclearion_inv_cnt_batch_details', value: args.details});
            rec.setValue({fieldId: 'custrecordclearion_inv_cnt_batch_note', value: args.note});
            rec.setValue({fieldId: 'name', value: `Count:${args.count_id}:${(new Date().getTime())}`});

            return rec.save();
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    /**
     *
     * @param {{location: number}} args
     * @returns {InventoryCount[]}
     * @constructor
     */
    GetInventoryCountsByLocation(args: { location: number }): InventoryCount[] {
        try {
            let counts: InventoryCount[] = [];

            // default the location if it is not provided
            const location = typeof args.location !== 'undefined'
                ? args.location
                : this.modules.runtime.getCurrentUser().location;

            if (typeof location !== 'number')
                throw `the location was not provided and we could not determine the location automatically`;

            this.modules.search.create({
                type: 'customrecordclearion_inv_count',
                filters: [
                    ['isinactive', 'is', 'F'],
                    'AND', ['custrecordclearion_inv_cnt_open_for_cnt', 'is', 'T'],
                    'AND', ['custrecordclearion_inv_cnt_location', 'is', location]
                ],
                columns: [
                    'name',
                    'custrecordclearion_inv_cnt_location',
                    'custrecordclearion_inv_cnt_open_for_cnt',
                    'custrecordclearion_inv_cnt_type',
                    'created',
                ]
            }).run().each((r: searchResult) => {
                if (r) {
                    counts.push({
                        id: parseInt(r.id),
                        created: r.getValue('created'),
                        name: r.getValue('name'),
                        type: r.getText('custrecordclearion_inv_cnt_type'),
                        location: r.getText('custrecordclearion_inv_cnt_location')
                    });
                }
                return true;
            });

            return counts;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    /**
     *
     * @param id
     * @constructor
     */
    GetAddressFromID(id: number) {
        try {
            var lookup = this.modules.search.lookupFields({
                type: "address",
                id: id,
                columns: [
                    "address", "address1", "address2", "address3", "city", "state",
                    "zip", "phone", "attention", "countrycode", "addressee"
                ]
            });

            var addr: any = {};
            addr.addressee = lookup["addressee"];
            addr.address1 = lookup["address1"];
            addr.address2 = lookup["address2"];
            addr.address3 = lookup["address3"];
            addr.attention = lookup["attention"];
            addr.city = lookup["city"];
            addr.state = lookup["state"];
            addr.zip = lookup["zip"];
            addr.country = lookup["countrycode"];
            addr.phone = lookup["phone"];

            return addr;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetShippingLabels(args: { order: Order }): { type: string, data: string }[] {
        try {
            let order = args.order;
            let labels = [];
            let fulfillments: {
                cartons: number,
                quantity: number,
                sscc: string,
                item: string,
                shipgroup: string,
                shipmethod: string
            }[] = [];

            // 1. gather the zpl label urls on the if's
            let columns = COMMON_TRANSACTION_COLUMNS;
            columns.push('custbodyclearion_sa_label_data');
            columns.push('custbodyclearion_box_tote_id');
            columns.push('quantity');
            columns.push('shipmethod');
            columns.push('shipgroup');
            columns.push('createdfrom');

            this.modules.search.create({
                type: 'transaction',
                columns: columns,
                filters: [
                    ['createdfrom', 'is', order.internalid],
                    'AND', ['mainline', 'is', 'T'],
                    'AND', ['recordtype', 'is', 'itemfulfillment'],
                ],
            }).run().each((r: searchResult) => {
                if (r) {
                    let labelDataUrl = r.getValue('custbodyclearion_sa_label_data');

                    fulfillments.push({
                        cartons: 1,
                        quantity: parseInt(r.getValue('quantity')) || 1,
                        item: r.getText('item'),
                        sscc: r.getValue('custbodyclearion_box_tote_id'),
                        shipgroup: r.getValue('shipgroup'),
                        shipmethod: 'FedEX'
                    });

                    if (Utils.isipaddress(labelDataUrl)) labels.push({
                        type: 'url',
                        data: labelDataUrl
                    });
                }
                return true;
            });

            // 2. gather the labels defined on the customer record if the entity is defined on this order
            if (order.entity) {
                // custentityclearion_assigned_label_formats
                let customerArr = this.modules.search.lookupFields({
                    type: "customer",
                    id: order.entity,
                    columns: ['custentityclearion_assigned_label_formats']
                });

                // load the global shipping label template
                const labelFormatFields = this.modules.search.lookupFields({
                    type: 'customrecordclearion_lf',
                    id: this.config.GLOBAL_SHIPPING_LABEL_ID,
                    columns: ['custrecordclearion_lf_code', 'name'],
                });

                if (typeof labelFormatFields['custrecordclearion_lf_code'] === 'undefined')
                    throw `could not lookup the "Global Shipping Labels" template by id: ${this.config.GLOBAL_SHIPPING_LABEL_ID}`;

                // initialize the template renderer
                let renderer = this.modules.render.create();
                renderer.templateContent = Utils.removeHtmlEntities(labelFormatFields['custrecordclearion_lf_code']);

                const customerLabelFormats: {
                    value: any,
                    text: string
                }[] = customerArr['custentityclearion_assigned_label_formats'];

                // iterate thru the order and remove any keys that have a currency symbol in the name. when we are done we need to
                // re-assign the updated collections back on to the parent item
                const proxyOrder = order;
                proxyOrder.items = order.items.map((i) => {
                    delete i['$id'];
                    return i;
                });
                proxyOrder.shipgroups = order.shipgroups.map((i) => {
                    delete i['$id'];
                    return i;
                });

                // lookup the address of the send by using the current location
                const senderAddr: ShippingAddress = this.getFulfillerInstance().getShippingAddressByLocation();

                if (Array.isArray(customerLabelFormats)) {
                    for (const format of customerLabelFormats) {
                        if (this.config.SUPPORTED_CUSTOM_LABELS.lastIndexOf(format.text) > -1) {
                            // depending on the format of the customer label, we may need to adjust thee template data source
                            switch (format.text) {
                                case 'UCC-128 Label':
                                case 'Cust Pickup Label':
                                    // set item to 'mixed skus' if more than one.
                                    let item = fulfillments.length > 1 ? 'Mixed SKU\'s' : fulfillments[0].item;

                                    // generate a ucc label for each item fulfillment (shipment)
                                    for (let i = 0; i < fulfillments.length; i++) {

                                        // shorthand the fulfillment
                                        let fulfillment = fulfillments[i];

                                        // lookup the to address by paring the shipgroup on the fulfillment
                                        // with the shipgroup on the order
                                        order.shipgroups.forEach((sg: ShipGroup) => {
                                            if (sg.id.toString() === fulfillment.shipgroup.toString()) {
                                                fulfillment['shipattention'] = sg.shipattention;
                                                fulfillment['shipaddress1'] = sg.shipaddress1;
                                                fulfillment['shipaddress2'] = sg.shipaddress2;
                                                fulfillment['shipcity'] = sg.shipcity;
                                                fulfillment['shipstate'] = sg.shipstate;
                                                fulfillment['shipzip'] = sg.shipzip;
                                                fulfillment['shipmethod'] = sg.item;
                                            }
                                        });

                                        // apply the data to the data source
                                        renderer.addCustomDataSource({
                                            format: this.modules.render.DataSource.JSON,
                                            alias: 'JSON_STR',
                                            data: JSON.stringify({
                                                LabelName: format.text,
                                                Order: proxyOrder,
                                                Shipment: fulfillment,
                                                Item: {item: item},
                                                AddrSender: senderAddr
                                            })
                                        });

                                        labels.push({
                                            type: format.text,
                                            data: renderer.renderAsString(),
                                        });
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            return labels;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    LookupOrderDetails(args: {
        id?: number,
        tranid?: string,
        append_items?: boolean,
        append_shipments?: boolean
        append_addresses?: boolean
    }): Order {
        try {
            if (typeof args.id === 'undefined' && typeof args.tranid === 'undefined')
                throw `you must provide an id or a document number to this function`;

            args.append_items = typeof args.append_items === 'undefined' ? false : args.append_items;
            args.append_shipments = typeof args.append_shipments === 'undefined' ? false : args.append_shipments;

            // make sure the id is accepted as a number if it is provided
            args.id = typeof args.id === 'string' ? (parseInt(args.id) || 0) : args.id;

            // remove blank strings from tranid
            args.tranid = (typeof args.tranid === 'string' && !(args.tranid.trim()).length)
                ? null
                : typeof args.tranid !== 'undefined' ? args.tranid.trim() : null;

            // initialize the order object
            let order: Order = null;

            this.modules.search.create({
                type: 'transaction',
                columns: [
                    'internalid',
                    'currency',
                    'tranid',
                    'status',
                    'name',
                    // 'shipmethod',
                    'otherrefnum',
                    'ismultishipto',
                    'location',
                    'subsidiary',
                    'subsidiary.country',
                    'custbodyclearion_postmen_svc_opts'
                ],
                filters: [
                    [args.id ? 'internalid' : 'tranid', 'is', args.id || args.tranid],
                    'AND', ['mainline', 'is', 'T']
                ],
            }).run().each((r: searchResult) => {
                if (r) order = new Order(
                    r.recordType,
                    parseInt(r.id),
                    r.getValue('tranid'),
                    [],
                    r.getValue('status'),
                    r.getText('name') || null,
                    r.getValue('name') || null,
                    null,
                    r.getValue('otherrefnum') || null,
                    r.getValue('ismultishipto') || null,
                    r.getText('custbodyclearion_postmen_svc_opts') || null,
                    r.getValue('currency') || null,
                    r.getValue('location') || null,
                    r.getValue('subsidiary') || null,
                    r.getValue({name: 'country', join: 'subsidiary'}) || 'USA',
                );
                return true;
            });

            if (!order)
                throw `could not find an order with the ${args.id ? 'internalid' : 'tranid'} you provided`;

            // if this order is multi-ship and we declare the shipmethod column, it will error out and vice versa
            // to prevent this we need to get the core columns and then lookup the shipping fields
            let orderFields = this.modules.search.lookupFields({
                type: order.recordType,
                id: order.internalid,
                columns: [
                    'ismultishipto',
                    'shipmethod',
                    'custbodyclearion_sa_incoterm',
                ]
            });

            // append the incoterms if they exist
            order.incoterm = typeof orderFields['custbodyclearion_sa_incoterm'][0] !== "undefined"
                ? orderFields['custbodyclearion_sa_incoterm'][0]['text']
                : null;

            if (typeof orderFields['ismultishipto'] !== 'undefined') order.ismultishipto = orderFields['ismultishipto'];
            if (typeof orderFields['shipmethod'][0] !== 'undefined') order.shipmethod = orderFields['shipmethod'][0]['value'];

            // only append items if requested
            if (args.append_items) order.items = this.GetOrderItems({order: order});

            // only append the shipments if requested
            if (args.append_shipments) order.shipments = this.GetOrderShipments({order: order});

            // if this is not a multi ship and the entity is provided, lookup the addresses if desired
            if (!order.ismultishipto && (order.entity && args.append_addresses)) {

                const fulfiller = new ItemFulfiller(this.modules, this.config);
                const orderId = order.internalid;
                const type = order.recordType;
                const toAddRec = this.modules.record.load({type: order.recordType, id: order.internalid});

                order.shippingaddress_key = parseInt(toAddRec.getValue('shippingaddress_key')) || null;

                // throw an error if this is not a multi-ship and no address was supplied
                if (!order.shippingaddress_key && !order.ismultishipto) {
                    throw `could not lookup the recipient's address using the shippingaddress_key on the order ${order.tranid}`;
                }

                // the to address should never change dynamically
                order.to = fulfiller.getShippingAddressByID(order.shippingaddress_key);

                if (fulfiller.is3rdParty(type, orderId) || fulfiller.isDropShip(type, orderId)) {
                    // use the address of the customer
                    order.from = fulfiller.getShippingAddressByCustomerID(order.entity);
                } else {
                    // use the address of the location sending
                    order.from = fulfiller.getShippingAddressByLocation();
                }

                // check of the to and from addresses have not been populated
                if (!order.to) throw `could not determine the recipients's address on the order ${order.tranid}`;
                if (!order.from) throw `could not determine the senders's address on the order ${order.tranid}`;
            }

            // if this is a multi ship to then append the ship groups
            order.shipgroups = order.ismultishipto ? this.GetOrderShipGroups({order: order}) : [];

            // if this is not a multi-ship order then we must append the current address as the sole shipgroup
            if (!order.ismultishipto) {

                // initialize a new ship group and set the method to that of the order
                const shipGroup = new ShipGroup();
                shipGroup.shipmethod = order.shipmethod.toString();

                // lookup the address from the order ship key and use it to update the shipgroup fields
                const addr: AffiliateOrderAddr = this.GetAddressFromID(order.shippingaddress_key);
                shipGroup.shipattention = addr.attention;
                shipGroup.shipaddress1 = addr.address1;
                shipGroup.shipaddress2 = addr.address2;
                shipGroup.shipaddress3 = addr.address3;
                shipGroup.shipcity = addr.city;
                shipGroup.shipstate = addr.state;
                shipGroup.shipzip = addr.zip;
                shipGroup.id = 1; // always default the id to 1
                shipGroup.shipgroup = 1; // always default the id to 1

                // push the new sg to the stack
                order.shipgroups.push(shipGroup);
            }

            return order;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetOrderShipments(args: { order: Order }): Shipment[] {
        try {
            let shipments: Shipment[] = [],
                columns = COMMON_TRANSACTION_COLUMNS;

            // add fields specific to itemfulfillments
            columns.push('custbodyclearion_box_tote_id');
            columns.push('custbodyclearion_stock_box');
            columns.push('custbodyclearion_sa_label_data');
            columns.push('createdfrom');
            columns.push('shipgroup');
            columns.push('custbodyclearion_original_shipgroup');
            columns.push('quantitypicked');

            // stock box params
            columns.push('custbodyclearion_stock_box.salesdescription');
            columns.push('custbodyclearion_stock_box.custitemclearion_has_a_box');

            // add the custom stock box dimensions
            columns.push('custbodyclearion_stock_box_width');
            columns.push('custbodyclearion_stock_box_height');
            columns.push('custbodyclearion_stock_box_length');
            columns.push('custbodyclearion_stock_box_weight');
            columns.push('custbodyclearion_stock_box_type');
            columns.push('custbodyclearion_containe_status');

            this.modules.search.create({
                type: 'transaction',
                columns: columns,
                filters: [
                    ['createdfrom', 'is', args.order.internalid],
                    //'AND', ['mainline', 'is', 'F'],
                    'AND', ['cogs', 'is', 'F'],
                    'AND', ['shipping', 'is', 'F'],
                    'AND', ['taxline', 'is', 'F'],
                    'AND', ['recordType', 'is', 'itemfulfillment'],
                ],
            }).run().each((r: searchResult) => {
                if (r) {

                    const shipment = new Shipment(
                        r.recordType,
                        r.getValue('tranid'),
                        parseInt(r.id),
                        r.getText('createdfrom'),
                        r.getValue('status'),
                        r.getValue('custbodyclearion_box_tote_id'),
                        r.getText('custbodyclearion_stock_box'),
                        r.getValue('custbodyclearion_original_shipgroup'),
                        parseInt(r.getValue('custbodyclearion_stock_box')) || null,
                        parseFloat(r.getValue('custbodyclearion_stock_box_width')) || null,
                        parseFloat(r.getValue('custbodyclearion_stock_box_height')) || null,
                        parseFloat(r.getValue('custbodyclearion_stock_box_length')) || null,
                        r.getValue({join: 'custbodyclearion_stock_box', name: 'salesdescription'}),
                        r.getValue({join: 'custbodyclearion_stock_box', name: 'custitemclearion_has_a_box'}),
                        parseFloat(r.getValue('custbodyclearion_stock_box_weight')) || null,
                        r.getText('custbodyclearion_stock_box_type'),
                        r.getText('custbodyclearion_containe_status'),
                        r.getValue('custbodyclearion_sa_label_data'),
                    );

                    // append the shipment item reference
                    shipment.item = {
                        item: r.getText('item'),
                        qty: parseInt(r.getValue('quantity')),
                        quantityshiprecv: 0,
                        type: r.getValue({name: 'type', join: 'item'})
                    };

                    shipments.push(shipment);
                }
                return true;
            });

            return shipments;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetOrderItems(args: { order: Order }): Entity[] {
        try {
            let entities: Entity[] = [],
                columns = COMMON_TRANSACTION_COLUMNS,
                shipGroup: ShipGroup = null;

            // if this is a multi ship order, add the columns to lookup the shipmethod on the line item
            if (typeof args.order.ismultishipto !== 'undefined' && args.order.ismultishipto) {
                columns.push('shipmethod');
                columns.push('shipcity');
                columns.push('shipstate');
                columns.push('shipzip');
                columns.push('shipgroup');
                columns.push('shipaddress1');
                columns.push('shipaddress2');
                columns.push('shipaddress3');
                columns.push('shipaddressee');
                columns.push('shipattention');
            }

            // log the current columns to make sure the qty committed is being received
            this.modules.log.audit({title: 'GetOrderItems:columns', details: columns});

            this.modules.search.create({
                type: 'transaction',
                columns: columns,
                filters: [
                    ['internalid', 'is', args.order.internalid],
                    'AND', ['mainline', 'is', 'F'],
                    'AND', ['item.type', 'noneof', this.config.RESTRICTED_LINE_ITEM_TYPES],
                ],
            }).run().each((r: searchResult) => {
                if (r) {
                    this.modules.log.audit({title: "search r", details: r});

                    const entity = new Entity(
                        Clearion_ENTITY.LINE_ITEM,
                        r,
                        'item',
                        args.order.ismultishipto
                    );

                    entities.push(entity);
                }
                return true;
            });

            return entities;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    GetOrderShipGroups(args: { order: Order }): any {
        try {
            let groups: Entity[] = [],
                columns = COMMON_TRANSACTION_COLUMNS,
                shipGroup: ShipGroup = null;

            // if this is a multi ship order, add the columns to lookup the shipmethod on the line item
            if (typeof args.order.ismultishipto !== 'undefined' && args.order.ismultishipto) {
                columns.push('shipmethod');
                columns.push('shipcity');
                columns.push('shipstate');
                columns.push('shipzip');
                columns.push('shipgroup');
                columns.push('shipaddress1');
                columns.push('shipaddress2');
                columns.push('shipaddress3');
                columns.push('shipaddressee');
                columns.push('shipattention');
            }

            // log the current columns to make sure the qty committed is being received
            this.modules.log.audit({title: 'GetOrderItems:columns', details: columns});

            this.modules.search.create({
                type: 'transaction',
                columns: columns,
                filters: [
                    ['internalid', 'is', args.order.internalid],
                    'AND', ['mainline', 'is', 'F'],
                    'AND', ['item.type', 'noneof', ['TaxGroup']],
                ],
            }).run().each((r: searchResult) => {
                if (r) {
                    this.modules.log.audit({title: "search r", details: r});

                    const entity = new Entity(
                        Clearion_ENTITY.LINE_ITEM,
                        r,
                        'item',
                        true
                    );

                    // override the line id with the item id
                    entity.id = parseInt(r.getValue({name: 'item'}));
                    groups.push(entity);
                }
                return true;
            });

            // iterate thru the groups and get a ship group map
            let shipGroupAddrs = {};
            let shipGroups = {};
            let final = [];

            groups.forEach((ent, index) => {
                if (ent.type !== 'ShipItem') {
                    shipGroupAddrs[index] = {
                        shipmethod: ent.shipmethod,
                        shipcity: ent.shipcity,
                        shipstate: ent.shipstate,
                        shipzip: ent.shipzip,
                        shipaddress1: ent.shipaddress1,
                        shipaddress2: ent.shipaddress2,
                        shipaddress3: ent.shipaddress3,
                        shipattention: ent.shipattention,
                        shipgroup: ent.shipgroup,
                        item: null,
                        item_id: null,
                    };
                } else {
                    shipGroups[ent.item_id.toString()] = [ent.item, ent.item_id];
                }
            });

            Object.keys(shipGroupAddrs).forEach((s, index) => {
                let en: Entity = shipGroupAddrs[s];
                en.id = en.shipgroup;
                en.$id = Guid.newGuid();

                //
                if (en.shipgroup) {
                    // we need to throw an error id the shipmethod is not defined
                    if (typeof en.shipmethod === "undefined" || !en.shipmethod)
                        throw `There is no shipping method defined for shipgroup #${en.shipgroup}. Please make sure the shipping information is complete for all line items`;

                    en.item = shipGroups[en.shipmethod.toString()][0];
                    en.item_id = shipGroups[en.shipmethod.toString()][1];
                    let exists = false;

                    final.forEach((et) => {
                        if (et['shipgroup'] === en.shipgroup) exists = true;
                    });

                    if (!exists)
                        final.push(en);
                }
            });

            return final;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    /**
     * fulfills an order sent by a client application fulfillment request
     * @param args
     * @constructor
     */
    FulfillOrder(args: IFulfillRequest): any {
        try {
            // todo: this can be removed as we are no longer using the cache module. check for artifacts afterwards
            if (args.hasOwnProperty('cache_store_id')) this.config.CACHE_ID = args.cache_store_id;

            // make sure we don't try to fulfill more than the allowed limit
            if (args.terms.length > this.config.MAX_ITEMS_ALLOWED_TO_FULFILL)
                throw `you are trying to fulfill more item than the allowed max (${this.config.MAX_ITEMS_ALLOWED_TO_FULFILL}). you submitted (${args.terms.length}) items`;

            // check for order readiness flag
            if (args.check_readiness) {
                const orderReadiness = this.GetOrderFulfillmentReadiness(args);
                if (!orderReadiness.ready) throw orderReadiness.reasons;
            }

            // initialize a fulfillment manager instance
            const fulfiller = new ItemFulfiller(this.modules, this.config), self = this;

            // load the source record (salesorder) into scope so we can use it to lookup item details
            fulfiller.$parentRec = this.modules.record.load({
                type: 'salesorder',
                id: args.order_id
            });

            // shorthand the container on the order request
            let containers = [];

            // maintain a map of {itemfulfillment} to {shipgroup} to prevent 'You must have at least
            // one valid line item for this transaction' errors
            let shipGroupFulfillmentMap = {};

            // flag to determine if this order is a multi ship
            const ismultishipto: boolean = fulfiller.$parentRec.getValue('ismultishipto') || false;
            this.modules.log.audit({title: 'ismultishipto', details: ismultishipto});

            // make sure the containers are defined if enforced globally
            if (this.config.ENFORCE_CONTAINER) {
                if (!Array.isArray(args.containers))
                    throw `no containers array was provided in the request while ENFORCE_CONTAINER=true`;

                // iterate each container and fulfill
                for (let j = 0; j < args.containers.length; j++) {
                    const container = args.containers[j];
                    const shipgroup: number = container.shipgroup;

                    if (typeof shipgroup !== 'undefined' && !shipGroupFulfillmentMap.hasOwnProperty(shipgroup))
                        shipGroupFulfillmentMap[shipgroup] = null;

                    let newContainer = this.fulfillContainer(
                        fulfiller,
                        container,
                        args,
                        self,
                        args.return_cont,
                        ismultishipto,
                        shipGroupFulfillmentMap
                    );

                    shipGroupFulfillmentMap[shipgroup] = newContainer.item_fulfillment_id;
                    containers.push(newContainer);
                }
            }

            return args.return_cont ? containers : fulfiller.manifests;
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    };

    /**
     *
     * @param {{type: Clearion_ENTITY; term: string}} args
     * @returns {Entity}
     * @constructor
     */
    LookupEntity(args: { type: Clearion_ENTITY; term: string }): Entity {
        try {
            let type, filters, columns, entities: Entity[] = [];
            switch (args.type) {
                case Clearion_ENTITY.ITEM:
                    type = 'item';
                    filters = [
                        ['isinactive', 'is', 'F'], 'AND',
                        ['nameinternal', 'is', args.term]
                    ];
                    columns = [
                        'itemid',
                        'isserialitem',
                        'islotitem',
                        'averagecost',
                        'description',
                        'custitemclearion_has_a_box',
                        'custitemclearion_production_type',
                        'custitemclearion_width',
                        'custitemclearion_length',
                        'custitemclearion_height',
                        'custitemclearion_roll_sqft',
                        'weight',
                        'custitemclearion_selected_commodity_code',
                        'type',
                        'custitemclearion_commodity_code',
                        'averagecost',
                    ];
                    break;
                case Clearion_ENTITY.ROLL:
                    type = 'customrecordclearion_roll';
                    filters = [
                        ['isinactive', 'is', 'F'], 'AND',
                        ['name', 'is', args.term]
                    ];
                    columns = [
                        'name',
                        'custrecordclearion_roll_item',
                        'custrecordclearion_roll_item.description',
                        'custrecordclearion_roll_item.averagecost',
                        'custrecordclearion_roll_item.islotitem',
                        'custrecordclearion_roll_item.isserialitem',
                        'custrecordclearion_roll_item.custitemclearion_has_a_box',
                        'custrecordclearion_roll_item.custitemclearion_production_type',
                        'custrecordclearion_roll_item.custitemclearion_roll_sqft',
                        'custrecordclearion_roll_item.weight',
                        'custrecordclearion_roll_item.custitemclearion_selected_commodity_code',
                        'custrecordclearion_roll_item.type',
                        'custrecordclearion_roll_item.custitemclearion_roll_length',
                        'custrecordclearion_roll_item.custitemclearion_roll_width',
                        'custrecordclearion_roll_item.custitemclearion_width',
                        'custrecordclearion_roll_item.custitemclearion_length',
                        'custrecordclearion_roll_item.custitemclearion_height',
                        'custrecordclearion_roll_item.custitemclearion_commodity_code',
                        'custrecordclearion_roll_item.averagecost',
                    ];
                    break;
            }

            // perform the search and return the model
            this.modules.search.create({
                type: type,
                filters: filters,
                columns: columns,
            }).run().each((r: searchResult) => {
                this.modules.log.audit({title: 'found term:', details: r});
                if (r) entities.push(new Entity(args.type, r));
                return true;
            });

            // make sure we only get one result
            if (entities.length > 1)
                throw `More than one entity returned for ${args.type} ${args.term} were only (1) was expected; actually (${entities.length}) were returned. Aborting this operation!`;

            return entities[0];
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    };

    private updateFields(type: string, id: number, values: any): void {
        try {
            this.modules.record.submitFields({
                type: type,
                id: id,
                values: values
            });
        } catch (e) {
            let error = e.details || e.message || e.toString();
            throw error;
        }
    }

    private fulfillContainer(fulfiller: IFulfillmentManager,
                             container: Container,
                             args: IFulfillRequest,
                             self,
                             return_cont: boolean = false,
                             ismultishipto: boolean = false,
                             ship_group_map: any = null) {

        // if this is multiship order, make sure the shipgroup is provided on the container reference
        if (ismultishipto && (typeof container.shipgroup !== 'number' || !container.shipgroup)) {
            throw `the param {shipgroup} was provided on container reference id #${container.id}. this is required for multi ship orders`;
        }

        // transform an item fulfillment off of the parent order
        fulfiller.transformSalesOrder(
            args.order_id,
            false,
            null,
            ismultishipto ? container.shipgroup : null,
            ship_group_map
        );

        // set and short-hand the container in scope
        fulfiller.setContainer(container);
        const cont = fulfiller.getContainer();

        // setup annotations for the memo population
        let annotations = [];

        // the total # of items actually fulfilled for this pass
        let fulfilledCnt = 0;

        // iterate over the terms (items) and fulfill accordingly
        for (let i = 0; i < args.terms.length; i++) {
            const spec = args.terms[i], type = Clearion_ENTITY[spec.type.toUpperCase()];

            // make sure our recordType is valid
            if (typeof type === 'undefined')
                throw `unknown term type (${spec.type}) provided for term (${spec.term}) at index args.terms[${i}]`;

            // lookup the item and inflate into an entity
            const entity = this.LookupEntity({type: type, term: spec.term});

            // make sure the entity is defined
            if (!entity)
                throw `could not lookup the ${type} with the specs you provided: ${spec.term}. perhaps this record is inactive?`;

            // make sure the container id is defined if ENFORCE_CONTAINER=true
            if (this.config.ENFORCE_CONTAINER) {
                if (typeof spec.container_id !== 'string' || !spec.container_id)
                    throw `invalid or no container id provided for term (${spec.term}) at index args.terms[${i}]`;

                // get the container reference and add the item to the container
                if (spec.container_id === cont.id) {
                    cont.addItem(entity);

                    if (typeof spec.location === 'number') entity.setLocation(args.location);
                    if (typeof spec.qty === 'number') entity.setQty(spec.qty); else entity.setQty(1);
                    if (typeof spec.line === 'number') entity.setLine(spec.line);

                    // finally, fulfill this line item
                    fulfiller.fulfill(
                        entity,
                        type === Clearion_ENTITY.ROLL,
                        null,
                        ismultishipto
                    );

                    // increment the fulfilled item count
                    fulfilledCnt++;
                }
            }
        }

        // update the customs information on the container before we request it to be rated
        fulfiller.updateContainerCustomsInfo(cont);

        // initialize the address book for the easypost/postmen request
        let addressBook: IEasyPostAddressBook = {
            // if this is a multi ship order, use the address on the ship
            // group, otherwise get the body level address by id
            to_address: cont.ismultiship
                ? ShippingAddress.fromContainerShipGroup(cont)
                : fulfiller.getShippingAddressByID(fulfiller.$record.getValue('shippingaddress_key')),
            // set the default from address to the fulfiller's location
            from_address: fulfiller.getShippingAddressByLocation()
        };

        // initialize the easypost options
        let options: IEasyPostOptions = new class implements IEasyPostOptions {
            label_format = self.config.EASYPOST_DEFAULT_LABEL_TYPE;
        };

        // update the logistics (easypost currently) options print_custom params
        fulfiller.decorateLogisticsPrintOptions(
            options,
            fulfiller.$parentRec.getValue('tranid'),
            fulfiller.$parentRec.getValue('otherrefnum') || '',
            cont.carrier_service_name,
            cont.id,
        );

        // update the easypost options if this is 3rd party shipping
        if (fulfiller.is3rdParty()) {
            annotations.push('3Prty');

            const customerID: number = fulfiller.getCustomerID();
            if (!customerID) throw `this transaction does not have an [entity] to lookup a 3rd party account, therefore [Third Party/Collect Shipping] does not need to be set as the SHIPPING BILLING on this order`;
            const customerAddr: ShippingAddress = fulfiller.getShippingAddressByCustomerID(customerID);
            const account = fulfiller.get3rdPartyAccount(customerID, cont.carrier_name);

            options.bill_third_party_postal_code = account.postal_code;
            options.bill_third_party_account = account.account_number;
            options.bill_third_party_country = account.country;

            // set the buyer address to the customer since we are billing 3rd party
            addressBook.buyer_address = customerAddr;
        }

        // if this is a drop ship we need to set the from address as the customer's and not clearion's
        if (fulfiller.isDropShip()) {
            annotations.push('DrpShp');
            const customerAddr: ShippingAddress = fulfiller.getShippingAddressByCustomerID(fulfiller.getCustomerID());
            addressBook.return_address = customerAddr;
        }

        // request rates for our container
        const rates: EasyPostRate[] = fulfiller.getRatesForContainer(cont, addressBook, options);

        // iterate over the rates returned and select the one we need. we will purchase it
        // after the item fulfillment succeeds
        rates.forEach((rate: EasyPostRate) => {
            if (rate.service === cont.carrier_service_name) fulfiller.setRate(rate);
        });

        // make sure we matched a rate returned from the api with the one on the container as this can differ sometimes
        if (!fulfiller.containerHasBeenRated()) throw `the carrier name defined on the easy post mapping (${cont.carrier_service_name}) was not matched to any rate returned from easypost. please make sure the mapping's carrier name is typed correctly and is in all-caps ex: UPS and not ups`;

        // set the status of the fulfillment to packed
        fulfiller.setStatus(IFulfillmentStatus.PACKED);
        fulfiller.buildMemo(
            fulfilledCnt,
            annotations.length ? `[${annotations.join(', ')}]` : null
        );

        // finally, save the fulfillment in scope
        const savedID: number = fulfiller.save();

        // save the item fulfillment id to the container for reference. this will be needed by the FulfillOrder() function
        fulfiller.container.item_fulfillment_id = savedID;

        // by this step the fulfillment was successful so lets purchase the rate. we do this now as we cannot refund
        // a label immediately after generation with the easypost api. it has to be approved by the carrier
        fulfiller.purchaseRate();

        // update the fulfillment with the label information returned from the rate purchase
        fulfiller.updateShippingInformation();

        // set the status of the item fulfillment to shipped after we have tagged the shipping information successfully
        fulfiller.setStatus(IFulfillmentStatus.SHIPPED, savedID);

        // todo: if specified, we need to try to bill the sales order

        // only allow this function if we are in sandbox mode
        if (args.test_mode && this.modules.runtime.envType.toLowerCase() === 'sandbox') {
            try {
                fulfiller.$modules.record.delete({type: fulfiller.$type, id: fulfiller.id});
            } catch (e) {

            }
        } else {

        }

        // return the container for review purposes only. do not consume
        return fulfiller.getContainer();
    }

    /**
     *  utility function to generate a unique roll id
     * @param {boolean} return_as_str
     * @returns {any}
     */
    private createRollID(return_as_str: boolean = false): any {
        try {
            let d = [
                    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    [1, 2, 3, 4, 0, 6, 7, 8, 9, 5],
                    [2, 3, 4, 0, 1, 7, 8, 9, 5, 6],
                    [3, 4, 0, 1, 2, 8, 9, 5, 6, 7],
                    [4, 0, 1, 2, 3, 9, 5, 6, 7, 8],
                    [5, 9, 8, 7, 6, 0, 4, 3, 2, 1],
                    [6, 5, 9, 8, 7, 1, 0, 4, 3, 2],
                    [7, 6, 5, 9, 8, 2, 1, 0, 4, 3],
                    [8, 7, 6, 5, 9, 3, 2, 1, 0, 4],
                    [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
                ], p = [
                    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    [1, 5, 7, 6, 2, 8, 3, 0, 9, 4],
                    [5, 8, 0, 3, 7, 9, 6, 1, 4, 2],
                    [8, 9, 1, 6, 0, 4, 3, 5, 2, 7],
                    [9, 4, 5, 3, 1, 2, 6, 8, 7, 0],
                    [4, 2, 8, 6, 5, 7, 3, 9, 0, 1],
                    [2, 7, 9, 3, 8, 0, 6, 4, 1, 5],
                    [7, 0, 4, 6, 9, 1, 3, 2, 5, 8]
                ],
                inv = [0, 4, 3, 2, 1, 5, 6, 7, 8, 9];

            /**
             *
             * @param array
             * @returns {string[]}
             */
            let invArray = (array: any): string[] => {
                let proxy = array;

                if (Object.prototype.toString.call(proxy) == "[object Number]")
                    proxy = String(proxy);

                if (Object.prototype.toString.call(proxy) == "[object String]")
                    proxy = proxy.split("").map(Number);

                return proxy.reverse();
            };

            /**
             *
             * @param {string[]} array
             * @returns {number}
             */
            let generate = (array: string[]) => {
                let c = 0,
                    invertedArray = invArray(array),
                    i = 0;

                while (i < iniNum.length) {
                    c = d[c][p[((i + 1) % 8)][invertedArray[i]]];
                    i++;
                }

                return inv[c];
            };

            let seed = 190000,
                rec = this.modules.record.create({
                    type: 'customrecordclearion_roll_seqgen',
                    isDynamic: true,
                }),
                id = rec.save();

            this.modules.record.delete({id: id, type: 'customrecordclearion_roll_seqgen'});

            let iniNum = (seed + (id)).toString(),
                arrNum = [], i = 0;

            while (i < iniNum.length) {
                arrNum[i] = iniNum.substring(i, i + 1);
                i++;
            }

            let checkSum = generate(arrNum),
                rollId = iniNum + checkSum;

            let serialNumber: number = parseInt(rollId);

            if (isNaN(serialNumber))
                throw `could not generate a serial number (roll id). {iniNum} + {checkSum} resulted in: NaN`;

            if (!serialNumber)
                throw `could not generate a serial number (roll id). {iniNum} + {checkSum} resulted in null or zero`;

            return return_as_str ? String(serialNumber) : serialNumber;
        } catch (e) {
            throw e.details || e.message || e.toString();
        }
    }

    private createSecurityCode(): string {
        try {
            const generateGloballyUniqueIdentifier = (() => {
                const s4 = () => {
                    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                };

                return () => {
                    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
                };
            })();

            const uuid = generateGloballyUniqueIdentifier();
            let code = uuid.replace(/0/g, 'O').replace(/-/g, 'Z');
            code = code.toUpperCase().substring(28, 36);

            return code;
        } catch (e) {
            throw e.details || e.message || e.toString();
        }
    }

}

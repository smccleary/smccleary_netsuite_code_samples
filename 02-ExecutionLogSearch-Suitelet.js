/**
 *@NApiVersion 2.0
 *@NScriptType Suitelet
 *@NModuleScope Public
 *@NAmdConfig  /02-ExecutionLogSearch-Config.json
 */
define([
	'N/ui/serverWidget',
	'N/file',
	'N/log',
	'N/search',
	'N/record',
	'clearionUtils',
], function (
	ui,
	file,
	log,
	search,
	record,
	utils
) {

	function onRequest(context) {
		var form = ui.createForm({
			title: 'Search Script Logs',
		});

		/** @type {{timeframe: string, search: string, user: number, page: number, logtype: string, searchfield: string}} */
		var params = context.request.parameters;

		const fields = {
			search: form.addField({label: 'Search Term', id: 'search', type: ui.FieldType.TEXT}),
			searchfield: form.addField({
				label: 'Log Field to Search',
				id: 'searchfield',
				type: ui.FieldType.SELECT,
			}),
			user: form.addField({
				label: 'Restrict to User (Optional)',
				id: 'user',
				type: ui.FieldType.SELECT,
				source: 'employee'
			}),
			logtype: form.addField({
				label: 'Log Type',
				id: 'logtype',
				type: ui.FieldType.SELECT,
			})
		};

		fields.search.layoutType = ui.FieldLayoutType.STARTROW;
		fields.user.layoutType = ui.FieldLayoutType.NORMAL;

		fields.logtype.addSelectOption({value: "Debug", text: "Debug"});
		fields.logtype.addSelectOption({value: "Audit", text: "Audit"});
		fields.logtype.addSelectOption({value: "Error", text: "Error"});
		fields.logtype.addSelectOption({value: "System", text: "System"});
		fields.logtype.addSelectOption({value: "Emergency", text: "Emergency"});

		fields.searchfield.addSelectOption({value: "detail", text: "Detail"});
		fields.searchfield.addSelectOption({value: "title", text: "Title"});

		fields.search.isMandatory = true;
		fields.logtype.isMandatory = true;
		fields.searchfield.isMandatory = true;

		fields.search.defaultValue = typeof params.search === "undefined" ? null : params.search;
		fields.logtype.defaultValue = typeof params.logtype === "undefined" ? 'Debug' : params.logtype;
		fields.searchfield.defaultValue = typeof params.searchfield === "undefined" ? 'Detail' : params.searchfield;

		form.addSubmitButton({label: 'Search'});

		if (context.request.method === 'GET') {

		} else {

			const type = typeof params.logtype === "undefined" ? null : params.logtype;

			const src = search.load({
				id: 'customsearchclearion_script_logs_today'
			});

			var filters = src.filters;

			filters.push(search.createFilter({
				name: params.searchfield,
				operator: 'contains',
				values: params.search
			}));

			if (typeof params.user !== "undefined" && typeof params.user === "number") {
				filters.push(search.createFilter({
					name: 'user',
					operator: 'equalto',
					values: params.user
				}));
			}

			if (type) {
				filters.push(search.createFilter({
					name: 'formulatext',
					formula: '{type}',
					operator: 'is',
					values: type
				}));
			}

			var results = [];

			const detailLimit = 250;
			const charWidth = 50;

			src.filters = filters;

			src.run().each(function (r) {
				var detail = r.getValue('detail');
				results.push({
					type: r.getValue('type'),
					user: r.getValue('user'),
					title: r.getValue('title'),
					date: r.getValue('date'),
					detail: detail.length > detailLimit ? detail.substr(0, detailLimit) + '...' : detail,
					script: r.getValue({join: 'script', name: 'name'}),
					script_type: r.getValue('scripttype'),
					script_id: r.getValue({join: 'script', name: 'internalid'}),
					deploy_id: r.getValue({join: 'scriptDeployment', name: 'internalid'})
				});
			});

			// throw JSON.stringify(src.columns, null, 4);

			if (results.length) {
				utils.renderSublistFromJsonData(
					'Results',
					results,
					form,
					ui.SublistType.STATICLIST,
					'sublist_results',
					{
						type: function (log) {
							return '<code style="color: orangered;">' + log['type'] + '</code>';
						},
						detail: function (log) {
							var inc = 0;

							const d = log['detail'].split('').map(function (c) {
								inc++;
								if (inc >= charWidth) {
									inc = 0;
									return c + '<br/>';
								}
								return c;
							}).join('');

							const html = '' +
								'<code style="color: #006699;">' +
								'' + d + '' +
								'</code>';
							return html;
						}
					}
				);
			} else {
				(form.addField({
					label: 'Results',
					id: 'results',
					type: ui.FieldType.INLINEHTML
				})).defaultValue = '<p style="padding: 20px; color: red;font-family: sans-serif !important;">No script logs found...</p>';
			}
		}

		context.response.writePage(form);
	}

	return {
		onRequest: onRequest
	}
});

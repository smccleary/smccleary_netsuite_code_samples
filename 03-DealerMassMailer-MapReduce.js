/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 *@NAmdConfig  /03-DealerMassMailer-Config.json
 */
define(['N/email',
		'N/file',
		'N/render',
		'N/runtime',
		'clearionDealerConfClasses',
		'N/log',
		'N/search'
	],
	/**
	 *
	 * @param email {email}
	 * @param file {file}
	 * @param render {render}
	 * @param runtime {runtime}
	 * @param classes {*}
	 * @param log {log}
	 * @param search {search}
	 * @returns {{reduce: reduce, getInputData: getInputData, summarize: summarize, map: map}}
	 */
	function (email, file, render, runtime, classes, log, search) {

		const modules = {
			search: search,
			log: log
		};

		/** @type {{ops: RenderingOps, attendee: AttendeeRecord, event}} */
		const ctrl = {
			ops: classes.RenderingOps,
			event: new classes.Event(null, modules),
			attendee: new classes.AttendeeRecord(null, modules),
		};

		const onLoadScheduleHtml = function (attendee, event_id) {
			const customer = ctrl.attendee.load(attendee);
			const parameter = event_id;
			const html = ctrl.ops.onRenderEvent(ctrl.event.load(parameter), customer);
			return html;
		};

		function getInputData() {
			try {
				return JSON.parse(runtime.getCurrentScript().getParameter({name: "custscriptclearion_dcmr_jobs"}));
			} catch (e) {
				log.error("getInputData", e.message || e.details || e);
			}
		}

		function map(context) {
			try {
				/** @type {MapReduceJob} */
				const job = JSON.parse(context.value);
				const scheduleHtml = onLoadScheduleHtml(job.attendee_id, job.event_id);

				log.error("on job:", job.email);

				// email.send({
				// 	author: -5,
				// 	recipients: 'smccleary@clearion.com',// todo: remove after testing: job.email,
				// 	subject: job.title,
				// 	body: scheduleHtml,
				// 	attachments: [
				// 		file.load({id: "ClearionConf2019EmailedFiles/XDC19 - PPF Competition Rules.pdf"}),
				// 		file.load({id: "ClearionConf2019EmailedFiles/XDC19 - Flat Glass Competition Rules.pdf"}),
				// 		file.load({id: "ClearionConf2019EmailedFiles/XDC19 - PPF Competition Rules.pdf"}),
				// 	]
				// });

				log.error("--->", "job completed successfully");

				context.write(job.email, job);
			} catch (e) {
				log.error("map", e.message || e.details || e);
			}
		}

		function reduce(context) {

		}

		function summarize(summary) {
			var type = summary.toString();
			log.audit(type + ' Usage Consumed', summary.usage);
			log.audit(type + ' Number of Queues', summary.concurrency);
			log.audit(type + ' Number of Yields', summary.yields);
			var contents = '';
			summary.output.iterator().each(function (key, value) {
				contents += (key + ' ' + value + '\n');
				return true;
			});
		}

		return {
			getInputData: getInputData,
			map: map,
			reduce: reduce,
			summarize: summarize
		};
	});

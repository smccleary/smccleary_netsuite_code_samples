/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 * @NAmdConfig  /01-Suitelet-CustomerSignature-Config.json
 */
define([
		'N/record',
		'N/search',
		'N/runtime',
		'N/ui/serverWidget',
		'N/url',
		'N/file',
		'formUtils'
	],
	/**
	 * @param {record} record
	 * @param {search} search
	 * @param {runtime} runtime
	 * @param {serverWidget} ui
	 * @param {url} url
	 * @param {file} file
	 * @param {*} formUtils
	 */
	function (record, search, runtime, ui, url, file, formUtils) {

		/** @param context {context} */
		function onRequest(context) {

			/**
			 *
			 * @type {{action: string, transaction: number}}
			 */
			const params = context.request.parameters;

			const form = ui.createForm({
				title: 'Orders Pending Customer Pickup'
			});

			// add the client script
			form.clientScriptModulePath = '/01-Suitelet-CustomerSignature-Client.js';

			if (context.request.method === 'GET') {
				// todo: create the suitelet for this form
				// render the list of customer pickup orders
				// todo: doc: IF249646
				// fields:
				// custbodyclearion_cust_pickup_sig
				// custbodyclearion_cust_pickup_date

				switch (params.action) {
					case 'pickup':
						form.title = 'Customer Pickup Confirmation';

						// render the details form fields
						const fields = {
							form_customer: form.addField({
								id: 'form_customer',
								label: 'Customer',
								type: ui.FieldType.TEXT,
							}),
							form_tranid: form.addField({
								id: 'form_tranid',
								label: 'Order ID',
								type: ui.FieldType.TEXT,
							}),
							form_date: form.addField({
								id: 'form_date',
								label: 'Pickup Date',
								type: ui.FieldType.DATETIMETZ,
							}),
							form_signature_data: form.addField({
								id: 'form_signature_data',
								label: 'form_signature_data',
								type: ui.FieldType.LONGTEXT,
							}),
							form_transaction_id: form.addField({
								id: 'form_transaction_id',
								label: 'Transaction ID',
								type: ui.FieldType.TEXT,
							}),
							form_signature: form.addField({
								id: 'form_signature',
								label: 'Customer Signature',
								type: ui.FieldType.INLINEHTML,
							}),
						};

						// append the submit button
						form.addSubmitButton({label: "Submit"});

						// add buttons to clear the signature
						form.addButton({
							label: 'Clear Signature',
							functionName: 'onClearSignature',
							id: 'btn_clear_signature'
						});

						// lookup the tranid and the customer information
						var transaction = search.lookupFields({
							type: 'itemfulfillment',
							id: params.transaction,
							columns: [
								'tranid',
								'entity',
							],
						});

						// setup the display defaults
						fields.form_customer.updateDisplayType({
							displayType: ui.FieldDisplayType.DISABLED
						});

						fields.form_tranid.updateDisplayType({
							displayType: ui.FieldDisplayType.DISABLED
						});

						fields.form_transaction_id.updateDisplayType({
							displayType: ui.FieldDisplayType.DISABLED
						});

						fields.form_date.updateDisplayType({
							displayType: ui.FieldDisplayType.DISABLED
						});

						fields.form_signature_data.updateDisplayType({
							displayType: ui.FieldDisplayType.HIDDEN
						});

						// set the default data
						fields.form_customer.defaultValue = transaction['entity'][0]['text'];
						fields.form_date.defaultValue = new Date();
						fields.form_tranid.defaultValue = transaction['tranid'];
						fields.form_transaction_id.defaultValue = params.transaction;
						fields.form_signature.defaultValue = '' +
							'<div id="signatureparent"><p>Customer Signature</p><div id="signature" class="signature"></div></div>';
						break;
					default:
						// placeholder for the fulfillments list
						var fulfillments = [];

						// create the transaction search
						search.create({
							type: 'transaction',
							columns: [
								'datecreated', 'tranid', 'shipmethod', 'entity', 'createdfrom'
							],
							filters: [
								['mainline', 'is', 'T'], 'AND',
								['datecreated', 'within', 'thisweek'], 'AND',
								['formulatext: {shipmethod}', 'is', 'Cust Pick Up'], 'AND',
								['recordType', 'is', 'itemfulfillment']
								// ['custbodyclearion_cust_pickup_date', 'isempty', null]
							]
						}).run().each(function (r) {
							if (r) {
								fulfillments.push({
									id: parseInt(r.id),
									tranid: r.getValue('tranid'),
									datecreated: r.getValue('datecreated'),
									shipmethod: r.getText('shipmethod'),
									entity: r.getText('entity'),
									customer: parseInt(r.getValue('entity')),
									createdfrom: r.getText('createdfrom'),
								});
							}
							return true;
						});

						// compose the link to the signature page
						const urlSigPage = url.resolveScript({
							scriptId: runtime.getCurrentScript().id,
							deploymentId: runtime.getCurrentScript().deploymentId,
							returnExternalUrl: false
						});

						// these are the dynamic columns the will hold the links out to the detail page
						const sublistDynamicColumns = {
							pickup: function (fulfillment, p2, p3) {
								const actionUrl = '&action=pickup&transaction=' + fulfillment['id'] + '&customer=' + fulfillment['customer'];
								const html = '<a href="' + urlSigPage + actionUrl + '">Pickup</a>';
								return html;
							}
						};

						// add the dynamic columns to the fulfillments
						fulfillments = fulfillments.map(function (p) {
							Object.keys(sublistDynamicColumns).forEach(function (col_key) {
								p[col_key] = null;
							});
							return p;
						});

						// render the sublist data
						formUtils.renderSublistFromJsonData(
							'Orders Pending Pickup',
							fulfillments,
							form,
							ui.SublistType.STATICLIST,
							'sublist_packages',
							sublistDynamicColumns
						);
						break;
				}
			} else {

				// save the signature to an image and append to the item fulfillment
				// this will over write any current images set
				const signatureImg = file.create({
					name: '78787878.png',
					fileType: file.Type.PNGIMAGE,
					contents: params['form_signature_data'],
					folder: 1588992,
					isOnline: false
				});

				const imgId = signatureImg.save();

				// update the transaction and return a success message with a link back to the home screen
				record.submitFields({
					id: parseInt(params['form_transaction_id']),
					type: 'itemfulfillment',
					values: {
						custbodyclearion_cust_pickup_date: params['form_date'],
						custbodyclearion_cust_pickup_sig: imgId
					}
				});

				const msg = form.addField({
					id: '_msg',
					label: '_msg',
					type: ui.FieldType.INLINEHTML
				});

				form.title = 'Signature Accepted!'
				msg.defaultValue = '<div style="font-size: 18px;"><p>Thank you for your business!</p></div>';
			}

			context.response.writePage(form);
		}

		return {
			onRequest: onRequest
		};

	});

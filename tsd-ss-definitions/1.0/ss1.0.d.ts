declare interface SS1_RECORD {
    getValue: (field: string) => void;
    getId: () => number;
    getRecordType: () => string;
}

declare function nlapiSubmitField(type: string, id: number, fields: string|string[], values: string|string[], doSourcing?: boolean): void;
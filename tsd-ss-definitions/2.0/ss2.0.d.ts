declare interface util {
	isArray: (obj: any) => boolean;
	isBoolean: (obj: any) => boolean;
	isDate: (obj: any) => boolean;
	each: (iterable: any, callback: (args?: any) => any) => any;
}

declare interface transaction {

}

declare interface format {

}

declare interface http {
	CacheDuration: string;
	Method: string;
	ClientResponse: {
		body: string,
		code: number,
		headers: { [key: string]: string },
	};
	ServerRequest: {
		body: string,
		files: { [key: string]: any },
		headers: { [key: string]: any },
		method: string,
		parameters: { [key: string]: string | number },
		url: string,
		getLineCount: (args: { group: string }) => void,
		getSublistValue: (args: { group: string, name: string, line: string }) => void,
	};
	ServerResponse: {
		addHeader: (args: { name: string, value: string }) => void;
		getHeader: (args: { name: string }) => void;
		renderPdf: (args: { xmlString: string }) => void;
		sendRedirect: (args: {
			type: string,
			identifier: string,
			id: string,
			editMode: boolean,
			parameters: { [key: string]: string | number },
		}) => void;
		setCdnCacheable: (args: any) => void;
		setHeader: (args: any) => void;
		write: (args: any) => void;
		writeFile: (args: any) => void;
		writeLine: (args: any) => void;
		writePage: (args: any) => void;
	};
	get: (args: { url: string, headers: { [key: string]: string } }) => void;
	delete: (args: { url: string, headers: { [key: string]: string } }) => void;
	post: (args: { url: string, body: string | any, headers: { [key: string]: string } }) => void;
	put: (args: { url: string, body: string | any, headers: { [key: string]: string } }) => void;
	request: (args: { method: string, url: string, body: string | any, headers: { [key: string]: string } }) => void;
}

declare interface log {
	debug: (arg: { title: string, details: any } | any, opts?: any) => void;
	error: (arg: { title: string, details: any } | any, opts?: any) => void;
	audit: (arg: { title: string, details: any } | any, opts?: any) => void;
}

declare class sublist {
	addField: (args: {
		id: string,
		type: string,
		label: string,
		source?: string,
	}) => string;
	setSublistValue: (args: { id: string, line: number, value: any }) => sublist;
	addRefreshButton: () => void;
}

declare interface encode {
	convert: (args: {
		string: string,
		inputEncoding: string,
		outputEncoding: string
	}) => string;
	Encoding: {
		UTF_8: string,
		BASE_64: string,
	}
}

declare class record {
	id: number | string;
	attach: (args: {
		record: {
			type: string,
			id: number,
		},
		to: {
			type: string,
			id: number,
		},
	}) => void;
	create: (args: { type: string, isDynamic: boolean }) => record;
	load: (args: { type: string, id: number, isDynamic?: boolean }) => record;
	save: (args?: { enableSourcing: boolean, ignoreMandatoryFields: boolean }) => number;
	delete: (args: { type: string, id: number }) => void;
	setValue: (args: { fieldId: string, value: any, ignoreFieldChange?: boolean }) => void;
	getValue: (args: { fieldId: string } | string) => any;
	getText: (args: { fieldId: string } | string) => string;
	getLineCount: (args: { sublistId: string }) => number;
	getSublistValue: (args: {
		sublistId: string,
		fieldId: string,
		line: number
	}) => any;
	getSublistText: (args: {
		sublistId: string,
		fieldId: string,
		line: number
	}) => string;
	getSublists: () => string[];
	getSublistSubrecord: (args: { fieldId: string, sublistId: string, line: number }) => any;
	findSublistLineWithValue: (args: { fieldId: string, sublistId: string, value: any }) => any;
	getCurrentSublistSubrecord: (args: { fieldId: string, sublistId: string }) => any;
	getCurrentSublistValue: (args: { fieldId: string, sublistId: string }) => any;
	getCurrentSublistText: (args: { fieldId: string, sublistId: string }) => string;
	commitLine: (args: { sublistId: string }) => void;
	removeLine: (args: {
		sublistId: string,
		line: number,
		ignoreRecalc: boolean
	}) => void;
	removeSublistSubrecord: (args: { fieldId: string, sublistId: string, line: number }) => void;
	setCurrentSublistValue: (args: {
		fieldId: string,
		sublistId: string,
		ignoreFieldChange: boolean,
		value: any
	}) => void;
	selectLine: (args: { line: number, sublistId: string }) => void;
	selectNewLine: (args: { sublistId: string }) => void;
	removeSubrecord: (args: { fieldId: string }) => void;
	setSublistValue: (args: { fieldId: string, sublistId: string, line: number, value: any }) => void;
	transform: (args: { fromType: string, fromId: number, toType: string, isDynamic: boolean }) => void;
}

declare interface serverWidget {
	createForm: (args: { title: string }) => form;
	FieldType: {
		TEXT: string,
		EMAIL: string,
		TEXTAREA: string,
		INLINEHTML: string,
		SELECT: string,
		FLOAT: string,
		LONGTEXT: string,
	};
	FieldLayoutType: {
		NORMAL: string,
	};
	FieldBreakType: {
		STARTCOL: string,
	};
	SublistType: {
		LIST: string,
		INLINEEDITOR: string,
	};
	FieldDisplayType: {
		INLINE: string,
		HIDDEN: string,
	};
}

declare class form {
	addField: (args: { id: string, type: string, label: string }) => field;
	addSublist: (args: { id: string, type: string, label: string }) => sublist;
	addPageLink: (args: { type: string, title: string, url: string }) => void;
	addSubmitButton: (args: { label: string }) => void;
	addFieldGroup: (args: { id: string, label: string }) => void;
	updateDefaultValues: (args: { [key: string]: any }) => void;
}

declare class field {
	isMandatory: boolean;
	displaySize: {
		width: number,
		height: number
	};
	layoutType: string;
	breakType: string;
	defaultValue: string | number;
	updateDisplayType: (args: { displayType: string }) => void;
	addSelectOption: (args: { value: any, text: string }) => void;
}

declare class context {
	response: response;
	request: request;
	newRecord: record;
	oldRecord: record;
	type: string;
	UserEventType: {
		EDIT: string,
	}
}

declare interface request {
	parameters: any;
	method: string
}

declare interface response {
	writePage: (form: form) => void;
	write: (html: string) => void;
}

declare interface email {
	send: (args: { author: number, recipients: string | number | number[], subject: string, body: string }) => void;
}

declare class file {
	id: number;
	load: (args: { id: number }) => file;
	getContents: () => string;
	create: (args: {
		name: string,
		fileType: string,
		contents: string,
		folder: number,
		isOnline?: boolean,
	}) => file;
	Type: {
		WORD: string,
		PDF: string,
		HTMLDOC: string,
		XMLDOC: string,
		CSV: string,
	};
	Encoding: {
		UTF8: string,
	}
}

declare class user {
	id: number;
	location: number;
	department: number;
	email: string;
	name: string;
	role: string;
	roleCenter: string;
	roleId: string;
	subsidiary: number;
}

declare interface session {
	get: (name: string) => any;
	set: (name: string, value: any) => void;
}

declare class script {
	id: string;
	deploymentId: string;
	logLevel: string;
	percentComplete: number;
	bundleIds: number[];
}

declare interface runtime {
	getCurrentSession: () => session;
	getCurrentUser: () => user;
	getCurrentScript: () => {
		id: number,
		deploymentId: number,
		getParameter: (args: { name: string }) => string | number
	};
}

declare class searchResult {
	id: any;
	recordType: any;
	getValue: (args: { name: string, join?: string }) => any;
	getText: (args: { name: string, join?: string }) => string;
}

declare interface createdSearch {
	run: () => {
		each: (func: (result: searchResult) => boolean) => void;
	}
}

declare interface search {
	Type: {};
	create: (args: { type: string, columns: string[], filters?: any[] }) => createdSearch;
	lookupFields: (args: { type: string, id: number, columns: string[] }) => void;
	createFilter: (args: {
		name: string,
		operator: string,
		values: any[],
		join?: string
	}) => void;
	createColumn: (args: {
		name: string,
		summary: string,
		join?: string
	}) => void;
}

declare class mapSummary {
	dateCreated: any;
	seconds: any;
	usage: any;
	concurrency: any;
	yields: any;
	keys: any;
	errors: any[];
}

declare class mapContext {
	isRestarted: boolean;
	key: string;
	value: any;
	write: (key: string, value: any) => void;
}

declare class reduceContext {
	isRestarted: boolean;
	key: string;
	values: any[];
	write: (key: string, value: any) => void;
}

declare class summaryContext {
	dateCreated: any;
	seconds: any;
	usage: any;
	concurrency: any;
	yields: any;
	inputSummary: any;
	mapSummary: any;
	reduceSummary: any;
	output: {
		iterator: () => {
			each: (func: (key: string, value: string) => any) => void
		},
	};
	isRestarted: boolean;
}

declare interface task {
	TaskType: {
		MAP_REDUCE: string
	};
	create(args: { taskType: string }): void;
}

declare interface url {
	resolveRecord(args: {
		recordType: string,
		recordId: string
	}): void;
	resolveScript(args: {
		scriptId: string,
		deploymentId: string,
		returnExternalUrl: boolean,
	}): void;
}

declare class renderer {
	addCustomDataSource: (args: { format: string, alias: string, data: any }) => void;
	renderAsString: () => string;
}

declare interface render {
	DataSource: {
		OBJECT: string
	};
	templateContent: string;
	create: () => renderer;
}
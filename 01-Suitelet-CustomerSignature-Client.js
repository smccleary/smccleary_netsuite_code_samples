/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NAmdConfig  /ClearionPCS.config.json
 */
define([
		'N/error',
		'N/search',
		'N/record',
		'N/ui/message',
		'N/url',
		'N/https',
		'jSignature.min.noconflict',
	],
	/**
	 *
	 * @param error {error}
	 * @param search {search}
	 * @param record {record}
	 * @param message {message}
	 * @param url {url}
	 * @param https {https}
	 * @param jSignature {*}
	 * @returns {{onReturnToPreviousScreen: onReturnToPreviousScreen, onSetPicked: onSetPicked, pageInit: pageInit, fieldChanged: fieldChanged}}
	 */
	function (error, search, record, message, url, https, jSignature) {

		function onReturnToPreviousScreen() {
			window.history.back();
		}

		var allowSubmit = false;

		/** @param context {scriptContext} */
		function pageInit(context) {
			AddStyle(
				'https://system.na0.netsuite.com/core/media/media.nl?id=4617497&c=3737768_SB1&h=82c5cd39b709ce3177e4&_xt=.css',
				'head'
			);

			// we need to override the main submit event to collect the image data of the
			// signature and append it to a hidden form field
			jQuery('#main_form').submit(function (e) {

				const data = jQuery("#signature").jSignature("getData", "image");

				console.log(data);

				// set the hidden form field with the image data src
				jQuery("#form_signature_data").val(data[1]);

				// e.preventDefault();
			});

			setTimeout(function () {
				jQuery("#signature").jSignature();
			}, 300);
		}

		function onClearSignature() {
			jQuery("#signature").jSignature('reset');
		}

		/** @param context {scriptContext} */
		function fieldChanged(context) {

		}

		function setTimeout(aFunction, milliseconds) {
			var date = new Date();
			date.setMilliseconds(date.getMilliseconds() + milliseconds);
			while (new Date() < date) {

			}
			return aFunction();
		}

		function AddJavascript(jsname, pos) {
			var tag = document.getElementsByTagName(pos)[0];
			var addScript = document.createElement('script');
			addScript.setAttribute('type', 'text/javascript');
			addScript.setAttribute('src', jsname);
			tag.appendChild(addScript);
		}

		function AddStyle(cssLink, pos) {
			var tag = document.getElementsByTagName(pos)[0];
			var addLink = document.createElement('link');
			addLink.setAttribute('type', 'text/css');
			addLink.setAttribute('rel', 'stylesheet');
			addLink.setAttribute('href', cssLink);
			tag.appendChild(addLink);
		}

		return {
			onReturnToPreviousScreen: onReturnToPreviousScreen,
			pageInit: pageInit,
			fieldChanged: fieldChanged,
			onClearSignature: onClearSignature,
		};
	});
